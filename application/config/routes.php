<?php defined('BASEPATH') OR exit('No direct script access allowed');

// $route['contactanos'] = 'Email';

$route['default_controller'] 				= 'Page';
$route['curso/(:any)'] 						= 'Page/page_curso/$1';
$route['curso/(:any)/(:num)'] 		= 'Page/page_details/$1/$2';
$route['categoria/(:any)'] 			= 'Page/page_categoria/$1';
$route['^admin/'] 							= 'admin/dashboard/index/';
$route['^admin'] 							= 'admin/dashboard/index/';
$route['^login'] 							= 'Login';


/******************** ROUTES PARA EL ADMINISTRADOR ************************/
$route['^admin/'] 							= 'admin/dashboard/index/';
$route['^admin'] 							= 'admin/dashboard/index/';
//$route['^admin/detalle-curso'] 				= 'admin/detalle_curso/index';
//$route['^admin/detalle-curso/(:any)'] 		= 'admin/detalle_curso/index/$1';
//$route['^admin/detalle-curso/add'] 			= 'admin/detalle_curso/add';
//$route['^admin/detalle-curso/add/(:any)'] 	= 'admin/detalle_curso/add/$1';
//$route['^admin/detalle-curso/edit/(:any)'] 	= 'admin/detalle_curso/edit/$1';
/**************************************************************************/



$route['404_override'] 			= '';
$route['translate_uri_dashes'] 	= FALSE;
