<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');
    ?>
    <style type="text/css" media="screen">
        
    

.carousel-fade .carousel-item {
 opacity: 0;
 transition-duration: .6s;
 transition-property: opacity;
}

.carousel-fade  .carousel-item.active,
.carousel-fade  .carousel-item-next.carousel-item-left,
.carousel-fade  .carousel-item-prev.carousel-item-right {
  opacity: 1;
}

.carousel-fade .active.carousel-item-left,
.carousel-fade  .active.carousel-item-right {
 opacity: 0;
}

.carousel-fade  .carousel-item-next,
.carousel-fade .carousel-item-prev,
.carousel-fade .carousel-item.active,
.carousel-fade .active.carousel-item-left,
.carousel-fade  .active.carousel-item-prev {
 transform: translateX(0);
 transform: translate3d(0, 0, 0);
}
.carousel-inner .carousel-caption{
    top: 20% !important;
    width: 50%;
    text-align: left;
}
.carousel-inner h5{
    font-size: 60px;
    font-family: fantasy !important;
}


.carousel-item.img-div{
    height: 450px;
}
.div-ofrecemos.img-div{
    height: 200px;
}

</style>
</head>
<body class="bg-light">
    <?php
        $this->load->view('menu/header');
        $this->load->view('menu/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" >
    <?php
        $this->load->view('page/page_curso');
    ?>
    <style type="text/css" media="screen">
        .nav-custom>li>a{
            color: #777676;
            background: #ddd !important;
        }
        .nav-custom>li>a:hover{
            background: #f44336c4 !important;
            color: #000 !important;
        }
        .nav-custom>li>a.active{
            color: #000 !important;
            background: #F44336 !important;
            color: #eee !important;
        }
    </style>
</content>  
<?php
    $this->load->view('recursos/js');
?> 
<script type="text/javascript" charset="utf-8" async defer>

   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      e.target // newly activated tab
      e.relatedTarget // previous active tab
    })
</script>
<?php
    $this->load->view('footer/footer');
?>    
</body>

</html>