<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Laboratorio Sistemas</title>
</head>
<body>
	<div class="container">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="well well-sm">
	                <form class="form-horizontal" id="form-contact">
	                    <fieldset>
	                        <legend class="text-center header">Contáctanos</legend>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_nombres" name="txt_nombres" type="text" placeholder="Nombres" class="form-control" autofocus>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_apellidos" name="txt_apellidos" type="text" placeholder="Apellidos" class="form-control">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_email" name="txt_email" type="text" placeholder="E-mail" class="form-control">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
	                            <div class="col-md-8">
	                                <input id="txt_telefono" name="txt_telefono" type="text" placeholder="Teléfono celular" class="form-control">
	                            </div>
	                        </div>

	                        <div class="form-group">
	                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
	                            <div class="col-md-8">
	                                <textarea class="form-control" id="txta_mensaje" name="txta_mensaje" placeholder="Escríbenos tu consulta o inquietud." rows="7"></textarea>
	                            </div>
	                        </div>
	                        <div class="form-group">
	                            <div class="col-md-12 text-center">
	                                <button class="btn btn-danger btn-lg" id="btn-enviar-contact">Enviar</button>
	                            </div>
	                        </div>
						
	                    </fieldset>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
</body>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<style type="text/css" media="screen">
	.header {
	    color: #dc3545;
	    font-size: 27px;
	    padding: 10px;
	}

	.bigicon {
	    font-size: 35px;
	    color: #dc3545;
	}	
</style>
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '#btn-enviar-contact', function(event) {
			event.preventDefault();
			var dataForm = $("#form-contact").serialize();
			var nombres = $("#txt_nombres").val();
			var Apellidos = $("txt_apellidos").val();
			var email = $("#txt_email").val();
			var telefono = $("#txt_telefono").val();
			var mensaje  = $("#txta_mensaje").val(); 
			$.ajax({
				url: '<?=base_url()?>email/contact/',
				type: 'POST',
				dataType: 'json',
				data: {txt_nombres:nombres,txt_apellidos:Apellidos,txt_email:email,txt_telefono:telefono,txta_mensaje:mensaje},
			}).done(function(data) {
				console.log(data);
			}).fail(function(e) {
				console.log(e.responseText);
			});
		});
	});
</script>
</html>