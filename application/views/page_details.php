<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');
    ?>
    <style type="text/css" media="screen">
        .nav-custom>li>a{
            color: #777676;
            background: #ddd !important;
        }
        .nav-custom>li>a:hover{
            background: #f44336c4 !important;
            color: #000 !important;
        }
        .nav-custom>li>a.active{
            color: #000 !important;
            background: #F44336 !important;
            color: #eee !important;
        }
    </style>
</head>
<body class="bg-light">
    <?php
        $this->load->view('menu/header');
        $this->load->view('menu/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" >
    <?php
        $this->load->view('page/page_details');
    ?>
</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    $this->load->view('footer/footer');
?>    
</body>

</html>