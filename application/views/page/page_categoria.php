<div class="container bg-white">
   <div class="row">
      <div class="col-12 bg-dark">
         <div class="row p-5"">
            <div class="col-8 text-white">
               <div class="h4 text-uppercase font-weight-bold">
                  <?=$categoria['nombre_categoria'];?>
               </div>
               <p class="text-secondary"><?=$categoria['descripcion_categoria'];?>
               </p>
               <div>
                  <!-- <span class="p-1 bg-secondary text-uppercase">AVANZADO</span>
                  <span class="p-1 bg-secondary text-uppercase">8 horas</span> -->
                  <!-- <span class="p-1 text-warning text-uppercase">precio en promocion <del class="text-danger">S/.80.00</del> s/.50.00</span> -->
               </div>
            </div>
            <!-- <div class="col-4 text-white text-center align-self-center">
               <span class="h4">S/. 50.00</span>
               <div>
                  <span class="btn btn-danger">COMPRAR</span>
               </div>
            </div> -->
         </div>
      </div>
      <div class="col-12 mt-3 mb-3 p-0">
         <div class="row">
          <?php foreach ($cursos as $key => $value): ?>
                    <div class="col-4 position-relative mb-3">
                      <a href="<?=$value['url_curso'];?>" title="">
                        <img src="<?=$value['url_imagen'];?>" class="w-100 rounded" alt="">
                        <div class=" position-relative ">
                            <div class="btn btn-success btn-sm col-12 text-center align-self-center text-white font-weight-bold text-uppercase h-100">
                                     <?=$value['nombre_curso'];?>
                            </div>
                            
                        </div>
                      </a>                        
                    </div>
                  <?php endforeach ?> 
            <!-- <div class="col-4 position-relative mb-3">
                      <a href="<?=base_url();?>curso/razonamiento-matematico" title="">
                        <img src="http://argentinosenirlanda.ie/wp-content/uploads/2017/04/Renovar-la-visa-de-estudiante-en-Irlanda.jpg" class="w-100 rounded" alt="">
                        <div class=" position-relative ">
                            <div class="btn btn-success btn-sm col-12 text-center align-self-center text-white font-weight-bold text-uppercase h-100">
                              razonamiento matematico
                            </div>
                            
                        </div>
                      </a>
            </div>
            <div class="col-4 position-relative mb-3">
                      <a href="<?=base_url();?>curso/razonamiento-matematico" title="">
                        <img src="https://1733129623.rsc.cdn77.org/wp-content/uploads/2017/09/ni%C3%B1os-en-el-colegio.jpg" class="w-100 rounded" alt="">
                        <div class=" position-relative ">
                            <div class="btn btn-success btn-sm col-12 text-center align-self-center text-white font-weight-bold text-uppercase h-100">
                              razonamiento matematico
                            </div>
                            
                        </div>
                      </a>
            </div>
            <div class="col-4 position-relative mb-3">
                      <a href="<?=base_url();?>curso/razonamiento-matematico" title="">
                        <img src="http://hbanoticias.com/wp-content/uploads/2015/09/057395-docentes-que-no-aprueben-examen-reubicacion-permaneceran-nivel-previo-primer.jpg" class="w-100 rounded" alt="">
                        <div class=" position-relative ">
                            <div class="btn btn-success btn-sm col-12 text-center align-self-center text-white font-weight-bold text-uppercase h-100">
                              razonamiento matematico
                            </div>
                            
                        </div>
                      </a>
            </div>
            <div class="col-4 position-relative mb-3">
                      <a href="<?=base_url();?>curso/razonamiento-matematico" title="">
                        <img src="http://hbanoticias.com/wp-content/uploads/2015/09/057395-docentes-que-no-aprueben-examen-reubicacion-permaneceran-nivel-previo-primer.jpg" class="w-100 rounded" alt="">
                        <div class=" position-relative ">
                            <div class="btn btn-success btn-sm col-12 text-center align-self-center text-white font-weight-bold text-uppercase h-100">
                              razonamiento matematico
                            </div>
                            
                        </div>
                      </a>
            </div>
            <div class="col-4 position-relative mb-3">
                      <a href="<?=base_url();?>curso/razonamiento-matematico" title="">
                        <img src="http://hbanoticias.com/wp-content/uploads/2015/09/057395-docentes-que-no-aprueben-examen-reubicacion-permaneceran-nivel-previo-primer.jpg" class="w-100 rounded" alt="">
                        <div class=" position-relative ">
                            <div class="btn btn-success btn-sm col-12 text-center align-self-center text-white font-weight-bold text-uppercase h-100">
                              razonamiento matematico
                            </div>
                            
                        </div>
                      </a>
            </div>
            <div class="col-4 position-relative mb-3">
                      <a href="<?=base_url();?>curso/razonamiento-matematico" title="">
                        <img src="http://hbanoticias.com/wp-content/uploads/2015/09/057395-docentes-que-no-aprueben-examen-reubicacion-permaneceran-nivel-previo-primer.jpg" class="w-100 rounded" alt="">
                        <div class=" position-relative ">
                            <div class="btn btn-success btn-sm col-12 text-center align-self-center text-white font-weight-bold text-uppercase h-100">
                              razonamiento matematico
                            </div>
                            
                        </div>
                      </a>
            </div> -->
         </div>
      </div>
   </div>
</div>