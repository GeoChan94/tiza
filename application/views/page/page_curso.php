<div class="container bg-white">
   <div class="row">
      <div class="col-12 bg-dark">
         <div class="row p-5"">
            <div class="col-8 text-white">
               <div class="h4 text-uppercase font-weight-bold">
                  <?=$curso['nombre_curso'];?>
               </div>
               <p class="text-secondary"><?=$curso['descripcion_curso'];?>
               </p>
               <div>
                  <!-- <span class="p-1 bg-secondary text-uppercase">AVANZADO</span>
                  <span class="p-1 bg-secondary text-uppercase">8 horas</span> -->
                  <!-- <span class="p-1 text-warning text-uppercase">precio en promocion <del class="text-danger">S/.80.00</del> s/.50.00</span> -->
               </div>
            </div>
            <!-- <div class="col-4 text-white text-center align-self-center">
               <span class="h4">S/. 50.00</span>
               <div>
                  <span class="btn btn-danger">COMPRAR</span>
               </div>
            </div> -->
         </div>
      </div>
      <div class="col-12">
         <div class="row">
            <div class="col-8 border pt-2 pb-2">
               <ul class="nav nav-tabs nav-custom justify-content-center nav-pills nav-fill" id="myTab" role="tablist">
                  <li class="nav-item">
                     <a class="nav-item nav-link active font-weight-bold text-uppercase" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Presentacion</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-item nav-link font-weight-bold text-uppercase" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Contenido</a>
                  </li>
               </ul>
               <div class="tab-content pt-2 pb-2" id="myTabContent">
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  	<div class="h5 text-uppercase font-weight-bold pt-4  ">
	               		presentacion
	               	</div>
                     <img src="https://escuela.it/uploads/curso-de-html-express-2-26.jpg" alt=""  class="w-100">
                     <div>Este curso de HTML te enseñará todo lo que necesitas saber sobre el lenguaje de marcación, en un tiempo reducido, de modo que conozcas lo básico y necesario para abordar, seguidamente, el estudio de cualquier otra tecnología que requiera HTML como conocimiento de base.</div>
                     <div>Este curso de HTML te enseñará todo lo que necesitas saber sobre el lenguaje de marcación, en un tiempo reducido, de modo que conozcas lo básico y necesario para abordar, seguidamente, el estudio de cualquier otra tecnología que requiera HTML como conocimiento de base.</div>
                     <div><br></div>
                     <div><span style="font-size: 18px; font-weight: bold;">Aprendizaje de HTML como un medio</span></div>
                     <div><br></div>
                     <div>No hace mucho tiempo, aprender HTML era la única vía necesaria para crear un sitio web.&nbsp; Los estudiantes aprendían este lenguaje y tenían todo lo que hacía falta para publicar su propia página, personal o profesional. Estamos hablando de los inicios de la popularización de Internet.</div>
                     <div><br></div>
                     <div>Hoy se puede construir una web sin siquiera conocer HTML, a través de diferentes plataformas online y sin embargo ese hecho no significa que el conocimiento del lenguaje haya perdido valor. Muy al contrario, decenas de profesiones de las más demandadas requieren dominar HTML, como uno de sus primeros pilares de aprendizaje.&nbsp;</div>
                     <div><br></div>
                     <div>En este tiempo, HTML se ha convertido en un medio, para llegar a nuevas parcelas de conocimiento, cuando antes era un fin en sí mismo. Ante esta filosofía hemos elaborado el Curso de HTML Express.</div>
                     <div><br></div>
                     <div><span style="font-size: 18px; font-weight: bold;">Objetivo del curso de HTML</span></div>
                     <div><br></div>
                     <div>Nuestro objetivo es cubrir la etapa de aprendizaje de HTML en un tiempo reducido, centrándonos en aquellos conocimientos más relevantes para la mayoría de las personas que desean aprender el lenguaje.&nbsp;</div>
                     <div><br></div>
                     <div>En ese sentido, hemos creado un esquema de contenido que se centra más en explicar los usos correctos del lenguaje, su filosofía y buenas prácticas, que en el abordaje de manera detallada de cada etiqueta, atributo, etc. Paralelamente, debido a la evolución del lenguaje hace que en 2018 sea primordial estudiar las partes de HTML que verdaderamente se usarán en proyectos en la actualidad, dejando de lado las áreas que han caído en desuso.</div>
                     <div><br></div>
                     <div>En resumen, en el curso de HTML Express el estudiante podrá, de una manera práctica, aprender todo lo necesario para acometer cualquier tarea futura, con la garantía que el uso que haga en adelante de HTML será el más adecuado.&nbsp;</div>
                     <div><br></div>
                     <div><span style="font-weight: bold; font-size: 18px;">Contenido del curso de HTML Express</span></div>
                     <div><br></div>
                     <div>A continuación se puede encontrar un resumen de los puntos que se abordarán en las clases del curso.</div>
                     <div><br></div>
                     <div><span style="font-weight: bold;">Clase 1: Conceptos y filosofías importantes para aprender HTML</span></div>
                     <div>Manera correcta de usar el HTML y el CSS</div>
                     <div>Documento básico HTML</div>
                     <div>Etiquetas de bloque y etiquetas de línea</div>
                     <div>Qué es HTML5</div>
                     <div>Etiquetas semanticas</div>
                     <div><br></div>
                     <div><span style="font-weight: bold;">Clase 2: Etiquetas HTML</span></div>
                     <div>Elementos sencillos</div>
                     <div>- Párrafos, negritas, enlaces, imágenes...</div>
                     <div>Elementos compuestos por varias etiquetas</div>
                     <div>- Listas, tablas...</div>
                     <div>Etiquetas de cabecera</div>
                     <div>- Meta, viewport...</div>
                     <div><br></div>
                     <div><span style="font-weight: bold;">Clase 3: Formularios</span></div>
                     <div>Etiquetas para formularios</div>
                     <div>Validación de formularios HTML5</div>
                     <div>Envío de formularios</div>
                     <div><br></div>
                     <div><span style="font-weight: bold;">Clase 4: Conocimientos y Herramientas para desarrollo HTML</span></div>
                     <div>Colores RGB</div>
                     <div>Juegos de caracteres y caracteres especiales</div>
                     <div>Editores de código</div>
                     <div>Herramientas para desarrollo en los navegadores</div>
                     <div>Validadores de HTML</div>
                     <div><br></div>
                     <div><span style="font-weight: bold;">Clase 5: Composición de una página web</span></div>
                     <div>Estructura de contenido</div>
                     <div>Gestión de archivos externos y rutas</div>
                     <div>Primeros pasos con CSS: creación de un layout</div>
                     <div>Carga de Javascript</div>
                     <div><br></div>
                     <div><span style="font-weight: bold;">Clase 6: Extra</span></div>
                     <div>Dominios de Internet y espacios de hosting</div>
                     <div>Cómo subir archivos a un servidor</div>
                     <div>FTP, GIT</div>
                     <div>Wordpress</div>
                     <div>Ojo en la optimización</div>
                     <div>Siguientes pasos</div>
                     <div><br></div>
                     <p></p>
                  </div>
                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  	<div class="h5 text-uppercase font-weight-bold pt-4  ">
	               		contenido
	               	</div>
	               	<div class="row m-0">
                        <?php 
                        $mes=1;
                           foreach ($contenido_curso as $key => $value): ?>
                           <?php if (($key+1)%5==0 ||$key==0): ?>
                              <div class="col-12 bg-dark text-uppercase font-weight-bold text-white p-3 align-self-center">
                                 <span class="fa fa-chevron-circle-down text-danger"></span> 
                                 <span>mes <?=$mes;?></span>
                              </div>
                           <?php $mes++; endif ?>
                           <div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">                           
                           <a href="<?=$value['url_curso_video'];?>" title="" class="text-dark">
                              <span><?='semana '.$value['semana'];?></span><br>
                              <small><?=$value['descripcion_curso'];?></small>
                           </a>
                        </div>
                        <?php endforeach ?>
	               		
	               		
	               		<!-- <div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">
	               			<span>semana 2</span>
	               		</div>
	               		<div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">
	               			<span>semana 3</span>
	               		</div>
	               		<div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">
	               			<span>semana 4</span>
	               		</div>
	               		<div class="col-12 bg-dark text-uppercase font-weight-bold text-white p-3 align-self-center">
	               			<span class="fa fa-chevron-circle-down text-danger"></span> 
	               			<span>mes 2</span>
	               		</div>
	               		<div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">
	               			<span>semana 1</span>
	               		</div>
	               		<div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">
	               			<span>semana 2</span>
	               		</div>
	               		<div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">
	               			<span>semana 3</span>
	               		</div>
	               		<div class="col-12 border border-top-0 text-uppercase font-weight-bold p-3 align-self-center">
	               			<span>semana 4</span>
	               		</div> -->
	               	</div>
                  </div>
               </div>
            </div>
            <div class="col-4 pt-2 pb-2">
            	<div class=" font-weight-bold text-uppercase border border-top-0 border-left-0 border-right-0">
            		docente
            	</div>
            	<div class="row justify-content-center">
            		<div class="col-7 mt-4 mb-4">
            			<img src="https://cdn.pixabay.com/photo/2012/04/13/00/21/lady-31217_960_720.png" class="border rounded rounded-circle w-100" alt="">
            		</div>
            	</div>
            	<div class="text-center ">
            		<span class="font-weight-bold"> Miguel Angel Alvarez</span><br>
            		<span class="text-secondary">Especialidad Matematicas</span>
            	</div>
            	<div class=" font-weight-bold text-uppercase border border-top-0 border-left-0 border-right-0">
            		temas
            	</div>
            	<div class="mt-4 mb-4">
            		<span class="p-1 text-uppercase bg-secondary text-white">tema1</span>
            		<span class="p-1 text-uppercase bg-secondary text-white">tema2</span>
            	</div>
            	<div class=" font-weight-bold text-uppercase border border-top-0 border-left-0 border-right-0">
            		resumen
            	</div>
            	<div class="row mt-4 mb-4 text-center">
            		 <div class="col-4">
            		 	<span class="font-weight-bold h4">20</span><br>
            		 	<span class="text-secondary font-weight-bold text-uppercase">clases</span>
            		 </div>
            		 <div class="col-4">
            		 	<span class="font-weight-bold h4">12</span><br>
            		 	<span class="text-secondary font-weight-bold text-uppercase">horas</span>
            		 </div>
            		 <div class="col-4">
            		 	<span class="font-weight-bold h4">1</span><br>
            		 	<span class="text-secondary font-weight-bold text-uppercase">docentes</span>
            		 </div>
            	</div>

            </div>
         </div>
      </div>
   </div>
</div>