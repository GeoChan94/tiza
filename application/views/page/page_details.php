<div class="container bg-white">
   <div class="row mt-3 mb-3">
      <div class="col-12 bg-dark p-0">
         <!-- <iframe width="100%" height="515" src="https://www.youtube.com/embed/JyqD_zfXfi8?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
         <?php if ( @$archivos[0]['uri_archivo'] ): ?>
         <video width="100%" height="515" controls controlsList="nodownload">
           <source src="<?=site_url(URI_PATH_FILE.$archivos[0]['uri_archivo'])?>" type="video/mp4">
           <source src="movie.ogg" type="video/ogg">
           Your browser does not support the video tag.
         </video>            
         <?php else: ?>
            
         <?php endif ?>


      </div>
      <div class="col-12">
         <div class="row">
            <div class="col-8 border pt-2 pb-2">
               <ul class="nav nav-tabs nav-custom justify-content-center nav-pills nav-fill" id="myTab" role="tablist">
                  <li class="nav-item">
                     <a class="nav-item nav-link active font-weight-bold text-uppercase" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">informacion</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-item nav-link font-weight-bold text-uppercase" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">archivos</a>
                  </li>
               </ul>
               <div class="tab-content pt-2 pb-2" id="myTabContent">
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  	<div class="h5 text-uppercase font-weight-bold pt-4  ">
	               		presentacion
	               	</div>
                     Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                     tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                     quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                     consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                     cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                     proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	
                  </div>
                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                  	<div class="h5 text-uppercase font-weight-bold pt-4  ">
	               		archivos
	               	</div>
	               	<div class="row m-0">
	               		<div class="col-6 ">
	               			<div class="row m-0">
	               				<div class="col-12 border bg-light p-3 rounded">
	               					<div class="font-weight-bold">
			               				nombre PDF
			               			</div>
	               				</div>
	               			</div>
	               			
	               		</div>
	               		<div class="col-6 ">
	               			<div class="row m-0">
	               				<div class="col-12 border bg-light p-3 rounded">
	               					<div class="font-weight-bold">
			               				nombre PDF 2
			               			</div>
	               				</div>
	               			</div>
	               		</div>
	               	</div>
                  </div>
               </div>
            </div>
            <div class="col-4 pt-2 pb-2">
            	<div class=" font-weight-bold text-uppercase border border-top-0 border-left-0 border-right-0">
            		docente
            	</div>
            	<div class="row justify-content-center">
            		<div class="col-7 mt-4 mb-4">
            			<img src="https://cdn.pixabay.com/photo/2012/04/13/00/21/lady-31217_960_720.png" class="border rounded rounded-circle w-100" alt="">
            		</div>
            	</div>
            	<div class="text-center ">
            		<span class="font-weight-bold"> Miguel Angel Alvarez</span><br>
            		<span class="text-secondary">Especialidad Matematicas</span>
            	</div>
            	<!-- <div class=" font-weight-bold text-uppercase border border-top-0 border-left-0 border-right-0">
            		temas
            	</div>
            	<div class="mt-4 mb-4">
            		<span class="p-1 text-uppercase bg-secondary text-white">tema1</span>
            		<span class="p-1 text-uppercase bg-secondary text-white">tema2</span>
            	</div>
            	<div class=" font-weight-bold text-uppercase border border-top-0 border-left-0 border-right-0">
            		resumen
            	</div>
            	 -->

            </div>
         </div>
      </div>
   </div>
</div>