<?php echo form_open('admin/cliente/edit/'.$cliente['id_cliente'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_usuario" class="col-md-4 control-label">Usuario</label>
		<div class="col-md-8">
			<select name="id_usuario" class="form-control">
				<option value="">select usuario</option>
				<?php 
				foreach($all_usuarios as $usuario)
				{
					$selected = ($usuario['id_usuario'] == $cliente['id_usuario']) ? ' selected="selected"' : "";

					echo '<option value="'.$usuario['id_usuario'].'" '.$selected.'>'.$usuario['id_usuario'].'</option>';
				} 
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="nombres_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Nombres Cliente</label>
		<div class="col-md-8">
			<input type="text" name="nombres_cliente" value="<?php echo ($this->input->post('nombres_cliente') ? $this->input->post('nombres_cliente') : $cliente['nombres_cliente']); ?>" class="form-control" id="nombres_cliente" />
			<span class="text-danger"><?php echo form_error('nombres_cliente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="apellidos_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Apellidos Cliente</label>
		<div class="col-md-8">
			<input type="text" name="apellidos_cliente" value="<?php echo ($this->input->post('apellidos_cliente') ? $this->input->post('apellidos_cliente') : $cliente['apellidos_cliente']); ?>" class="form-control" id="apellidos_cliente" />
			<span class="text-danger"><?php echo form_error('apellidos_cliente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="email_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Email Cliente</label>
		<div class="col-md-8">
			<input type="text" name="email_cliente" value="<?php echo ($this->input->post('email_cliente') ? $this->input->post('email_cliente') : $cliente['email_cliente']); ?>" class="form-control" id="email_cliente" />
			<span class="text-danger"><?php echo form_error('email_cliente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="dni_cliente" class="col-md-4 control-label"><span class="text-danger">*</span>Dni Cliente</label>
		<div class="col-md-8">
			<input type="text" name="dni_cliente" value="<?php echo ($this->input->post('dni_cliente') ? $this->input->post('dni_cliente') : $cliente['dni_cliente']); ?>" class="form-control" id="dni_cliente" />
			<span class="text-danger"><?php echo form_error('dni_cliente');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>