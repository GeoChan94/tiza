<div class="pull-right">
	<a href="<?php echo site_url('admin/cliente/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Cliente</th>
		<th>Id Usuario</th>
		<th>Nombres Cliente</th>
		<th>Apellidos Cliente</th>
		<th>Email Cliente</th>
		<th>Dni Cliente</th>
		<th>Actions</th>
    </tr>
	<?php foreach($clientes as $c){ ?>
    <tr>
		<td><?php echo $c['id_cliente']; ?></td>
		<td><?php echo $c['id_usuario']; ?></td>
		<td><?php echo $c['nombres_cliente']; ?></td>
		<td><?php echo $c['apellidos_cliente']; ?></td>
		<td><?php echo $c['email_cliente']; ?></td>
		<td><?php echo $c['dni_cliente']; ?></td>
		<td>
            <a href="<?php echo site_url('cliente/edit/'.$c['id_cliente']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('cliente/remove/'.$c['id_cliente']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
