<nav class="navbar navbar-expand-lg  navbar-dark bg-dark">
  <a class="navbar-brand" href="<?=base_url();?>" target="_blank">TIZA</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="<?=base_url();?>admin/">Dashboard <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>admin/curso">cursos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>admin/docente">docentes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>admin/cliente">clientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>admin/categoria">categoria</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?=base_url();?>admin/duracion">duracion</a>
      </li>
     
      <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Config
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li> -->
    </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a  href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Admin
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="<?=base_url()?>login/logout">cerrar sesion</a>
          <!-- <a class="dropdown-item" href="#">Something else here</a> -->
        </div>
      </li>
      </ul>
  </div>
</nav>