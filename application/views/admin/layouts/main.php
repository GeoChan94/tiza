
<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');
        $this->load->view('recursos/js');
    ?>
</style>
</head>
<body class="bg-light">
    <?php
        $this->load->view('admin/menu/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" >
    	<div class="container">
    		<div class="row">
    			<div class="col-12 pt-3 pb-3">
    				<?php	if(isset($_view) && $_view)
					    $this->load->view($_view);
					?>
    			</div>
    		</div>
    		
    	</div>
	    
	</content>  
    <?php
        $this->load->view('footer/footer');
    ?>    
</body>

</html>