<h3>AGREGAR DOCENTE PARA EL CURSO <strong><?=ucwords(mb_strtolower(@$curso['nombre_curso'].' - '.@$detalle_curso['descripcion_curso']))?></strong></h3><hr/>
<?php echo form_open('admin/docente/add/'.@$id_detalle_curso.'/'.@$curso['id_curso'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="especialidad_docente" class="col-md-4 control-label"><span class="text-danger">*</span>Especialidad Docente</label>
		<div class="col-md-8">
			<select name="especialidad_docente" class="form-control">
				<option value="">select</option>
				<?php 
				$especialidad_docente_values = array(
					'0'=>'Seleccione...',
					'1'=>'Nombramiento Docente',
					'2'=>'PRE-U',
					'3'=>'Preparación para Concursos',
					'4'=>'Nivelación',
					'5'=>'Reforzamiento',
					'6'=>'Clases Particulares',
				);

				foreach($especialidad_docente_values as $value => $display_text)
				{
					$selected = ($value == $this->input->post('especialidad_docente')) ? ' selected="selected"' : "";

					echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('especialidad_docente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="nombres_docente" class="col-md-4 control-label"><span class="text-danger">*</span>Nombres Docente</label>
		<div class="col-md-8">
			<input type="text" name="nombres_docente" value="<?php echo $this->input->post('nombres_docente'); ?>" class="form-control" id="nombres_docente" />
			<span class="text-danger"><?php echo form_error('nombres_docente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="apellidos_docente" class="col-md-4 control-label"><span class="text-danger">*</span>Apellidos Docente</label>
		<div class="col-md-8">
			<input type="text" name="apellidos_docente" value="<?php echo $this->input->post('apellidos_docente'); ?>" class="form-control" id="apellidos_docente" />
			<span class="text-danger"><?php echo form_error('apellidos_docente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="telefono_docente" class="col-md-4 control-label"><span class="text-danger">*</span>Telefono Docente</label>
		<div class="col-md-8">
			<input type="text" name="telefono_docente" value="<?php echo $this->input->post('telefono_docente'); ?>" class="form-control" id="telefono_docente" />
			<span class="text-danger"><?php echo form_error('telefono_docente');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="email_docente" class="col-md-4 control-label"><span class="text-danger">*</span>Email Docente</label>
		<div class="col-md-8">
			<input type="text" name="email_docente" value="<?php echo $this->input->post('email_docente'); ?>" class="form-control" id="email_docente" />
			<span class="text-danger"><?php echo form_error('email_docente');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="<?=base_url()?>admin/docente/index/<?=@$detalle_curso['id_detalle_curso'].'/'.@$curso['id_curso']?>" class="btn btn-danger">Volver</a>
        </div>
	</div>

<?php echo form_close(); ?>