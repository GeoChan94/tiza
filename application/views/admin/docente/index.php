<h3>Docentes para <strong><?=ucwords(mb_strtolower(@$curso['nombre_curso'].' - '.@$detalle_curso['descripcion_curso']))?></strong></h3><hr/>
<div class="pull-left">
	<a href="<?php echo site_url('admin/detalle_curso/index/').@$curso['id_curso']?>" class="btn btn-danger">Volver a Cursos</a> 
</div>
<div class="pull-right">
	<a href="<?php echo site_url('admin/docente/add/'.@$id_detalle_curso.'/'.@$curso['id_curso']); ?>" class="btn btn-success">Agregar Docente</a> 
</div>

<?php
$especialidad_docente_values = array(
	'0'=>'Seleccione...',
	'1'=>'Nombramiento Docente',
	'2'=>'PRE-U',
	'3'=>'Preparación para Concursos',
	'4'=>'Nivelación',
	'5'=>'Reforzamiento',
	'6'=>'Clases Particulares',
);
?>
<table class="table table-striped table-bordered">
    <tr>
		<th>#</th>
		<th>Especialidad</th>
		<th>Nombres</th>
		<th>Telefono</th>
		<th>Email</th>
		<th>Actions</th>
    </tr>
	<?php foreach($docentes as $key => $d ){ ?>
    <tr>
		<td><?php echo ++$key ?></td>
		<td><?php echo $especialidad_docente_values[$d['especialidad_docente']]; ?></td>
		<td><?php echo $d['nombres_docente'].' '.$d['apellidos_docente']; ?></td>
		<td><?php echo $d['telefono_docente']; ?></td>
		<td><?php echo $d['email_docente']; ?></td>
		<td>
			<div class="btn-group">
	            <a href="<?php echo site_url('admin/docente/edit/'.$d['id_docente'].'/'.@$id_detalle_curso.'/'.@$curso['id_curso']); ?>" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a> 
	            <a href="javascript:void(0)" data-id="<?=$d['id_docente']?>" class="btn btn-danger btn-xs btn-eliminar" title="Eliminar"><i class="fa fa-remove"></i></a>
	        </div>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>


<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.btn-eliminar', function(event) {
			event.preventDefault();
			var id = $(this).data('id');
			console.log(id);
		});
	});
</script>