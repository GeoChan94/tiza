<?php echo form_open('admin/categoria/edit/'.$categoria['id_categoria'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="nombre_categoria" class="col-md-4 control-label">Nombre Categoria<span class="text-danger">*</span></label>
		<div class="col-md-8">
			<input type="text" name="nombre_categoria" value="<?php echo ($this->input->post('nombre_categoria') ? $this->input->post('nombre_categoria') : $categoria['nombre_categoria']); ?>" class="form-control" id="nombre_categoria" />
			<span class="text-danger"><?php echo form_error('nombre_categoria');?></span>
		</div>
	</div>
    <div class="form-group">
        <label for="url_categoria" class="col-md-4 control-label">Url Categoria<span class="text-danger">*</span></label>
        <div class="col-md-8">
            <input type="text" name="url_categoria" value="<?php echo ($this->input->post('url_categoria') ? $this->input->post('url_categoria') : $categoria['url_categoria']); ?>" class="form-control" id="url_categoria" />
            <span class="text-danger"><?php echo form_error('url_categoria');?></span>
        </div>
    </div>
	<div class="form-group">
		<label for="descripcion_categoria" class="col-md-4 control-label">Descripción Categoria</label>
		<div class="col-md-8">
			<textarea name="descripcion_categoria" class="form-control" id="descripcion_categoria"><?php echo ($this->input->post('descripcion_categoria') ? $this->input->post('descripcion_categoria') : $categoria['descripcion_categoria']); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion_categoria" class="col-md-4 control-label">Imágen Categoria</label>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-8">
					<div class="input-group">
						<div class="input-group-prepend">
						    <div class="btn btn-primary fileUpload">
					        	<span><i class="fa fa-upload" aria-hidden="true"></i> Upload</span>
					            <input id="archivos" name="archivos"  type="file" multiple="false" class="file archivos upload" value="Seleccionar imágen...">
					        </div> 
						</div>
						<input type="text" name="imagen_categoria" value="<?php echo ($this->input->post('imagen_categoria') ? $this->input->post('imagen_categoria') : $categoria['imagen_categoria']); ?>" class="form-control" id="imagen_categoria" readonly placeholder="Seleccione una imágen"/>
					</div>
					<span class="text-danger"><?php echo form_error('imagen_categoria');?></span>
				</div>
				<div class="col-md-4">
					<div class="imagencargada text-center" id="imagencargada"><img src="<?=base_url()?>assets/archivos/<?=$categoria['imagen_categoria']?>" width="160" height="90" title="Imagen Representativa del Servicio" class="img-thumbnail"></div>
				</div>
			</div>
	    </div>
	</div><hr/>
	<div class="col-sm-offset-4 col-sm-8">
		<button type="submit" class="btn btn-success">Guardar</button>
		<a href="<?=base_url()?>admin/categoria" class="btn btn-danger">Volver</a>
    </div>
	
<?php echo form_close(); ?>
<script>
jQuery(document).ready(function($) {
	    var fileExtension = "";
    $(':file').change(function(){
        var file = $("#archivos")[0].files[0];
        var fileName = file.name;
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        var fileSize = file.size;
        var fileType = file.type;
        //showMessage("<span class='info'>Archivo para subir: "+fileName+", "+fileSize+" bytes.</span>");
        var formData = new FormData($("#form-add-categoria")[0]);
        var message = ""; 
        $.ajax({
            url: '<?=base_url()?>assets/file.php',  
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("#imagencargada").empty();
                $("#imagencargada").append('<img src="<?=base_url()?>assets/img/loading.gif" width="25%"/>');         
            },
            success: function(data){
                var rpta = $.parseJSON(data);
                //message = $("<span class='success'>La imagen ha subido correctamente.</span>");
                if(isImage(fileExtension)){
                    if ( rpta['rpta'] === 'success' ) {
                        $("#imagen_categoria").empty().val(rpta['imagen']);
                        $("#imagen").prop("readonly",true);                        
                    }
                    $("#imagencargada").empty().append('<img src="' + rpta['url'] + '" width="160" height="90" title="Imagen Representativa del Servicio" class="img-thumbnail">'); 
                }
            },
            error: function(){
                //message = $("<span class='error'>Ha ocurrido un error.</span>");
                //showMessage(message);
            }
        });
    });
 
    function isImage(extension){
        switch(extension.toLowerCase()) {
            case 'jpg': case 'gif': case 'png': case 'jpeg':
                return true;
            break;
            default:
                return false;
            break;
        }
    }
});
</script>
<style type="text/css">
    .fileUpload {
    position: relative;
    overflow: hidden;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>