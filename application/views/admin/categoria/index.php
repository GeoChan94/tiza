<div class="pull-right">
	<a href="<?php echo site_url('admin/categoria/add'); ?>" class="btn btn-success">Nueva Categoria</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>#</th>
		<th>Categoria</th>
		<th>Descripción</th>
		<th>Actions</th>
    </tr>
	<?php foreach($categorias as $key => $c){ ?>
    <tr>
		<td><?php echo ++$key; ?></td>
		<td><?php echo mb_strtoupper($c['nombre_categoria']); ?></td>
		<td><?php echo mb_strtoupper($c['descripcion_categoria']); ?></td>
		<td>
        	<div class="btn-group">
	            <a href="<?php echo site_url('admin/categoria/edit/'.$c['id_categoria']); ?>" class="btn btn-info btn-xs" title="Editar"><i class="fa fa-pencil"></i></a> 
	            <a href="javascript:void(0)" data-id="<?=$c['id_categoria']?>" class="btn btn-danger btn-xs btn-eliminar" title="Eliminar"><i class="fa fa-remove"></i></a>
			</div>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.btn-eliminar', function(event) {
			event.preventDefault();
			var id = $(this).data('id');
			console.log(id);
		});
	});
</script>