<h3>Agregar Horario a <strong><?=ucwords(mb_strtolower(@$curso['nombre_curso'].' - '.$detalle_curso['descripcion_curso']))?></strong></h3><hr/>
<?php echo form_open('admin/horario/add/'.@$id_detalle_curso.'/'.@$curso['id_curso'],array("class"=>"form-horizontal")); ?>
	<div class="form-group">
		<label for="nombre_horario" class="col-md-6 control-label"><span class="text-danger">*</span>Nombre Horario (Ejemplo: Tardes, Verano vacional, etc)</label>
		<div class="col-md-8">
			<input type="text" name="nombre_horario" value="<?php echo $this->input->post('nombre_horario'); ?>" class="form-control" id="nombre_horario" />
			<span class="text-danger"><?php echo form_error('nombre_horario');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="cantidad_horario" class="col-md-6 control-label"><span class="text-danger">*</span>Cantidad Horario (Ejemplo: 3  = 3 semanas, dias, meses, etc)</label>
		<div class="col-md-8">
			<input type="text" name="cantidad_horario" value="<?php echo $this->input->post('cantidad_horario'); ?>" class="form-control" id="cantidad_horario" />
			<span class="text-danger"><?php echo form_error('cantidad_horario');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_horario" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Horario</label>
		<div class="col-md-8">
			<select name="tipo_horario" class="form-control">
				<option value="">select</option>
				<?php 
				$tipo_horario_values = array(
					'0'=>'Seleccione...',
					'1'=>'Minutos',
					'2'=>'Horas',
					'3'=>'Días',
					'4'=>'Semanas',
					'5'=>'Meses',
					'6'=>'Años',
					'7'=>'Trimestres',
					'8'=>'Bimestres',
				);

				foreach($tipo_horario_values as $value => $display_text)
				{
					$selected = ($value == $this->input->post('tipo_horario')) ? ' selected="selected"' : "";

					echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_horario');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="<?=base_url()?>admin/horario/index/<?=@$id_detalle_curso.'/'.@$curso['id_curso']?>" class="btn btn-danger">Volver</a>
        </div>
	</div>

<?php echo form_close(); ?>