<h3>HORARIOS DEL CURSO <?=mb_strtoupper($curso['nombre_curso'].' - '.$detalle_curso['descripcion_curso'])?></h3><hr />
<div class="pull-right">
	<a href="<?php echo site_url('admin/horario/add/'.@$id_detalle_curso.'/'.@$id_curso)?>" class="btn btn-success">Agregar Horario</a> 
</div>
<div class="pull-left">
	<a href="<?=base_url()?>admin/detalle_curso/index/<?=@$id_curso?>" class="btn btn-danger">Volver</a>
</div>
<?php
$tipo_horario_values = array(
	'1'=>'Minutos',
	'2'=>'Horas',
	'3'=>'Días',
	'4'=>'Semanas',
	'5'=>'Meses',
	'6'=>'Años',
	'7'=>'Trimestres',
	'8'=>'Bimestres',
);

?>
<table class="table table-striped table-bordered">
    <tr>
		<th>#</th>
		<th>Nombre Horario</th>
		<th>Horas</th>
		<th>Actions</th>
    </tr>
	<?php foreach($horarios as $key => $h){ ?>
    <tr>
		<td><?php echo ++$key; ?></td>
		<td><?php echo $h['nombre_horario']; ?></td>
		<td><?php echo $h['cantidad_horario'].' '.$tipo_horario_values[$h['tipo_horario']]; ?></td>
		<td>
            <a href="<?php echo site_url('admin/horario/edit/'.$h['id_horario'].'/'.@$id_detalle_curso.'/'.@$id_curso); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/horario/remove/'.$h['id_horario']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
