<h3 class="text-center"><strong>CURSO <?=mb_strtoupper(@$curso['nombre_curso'].' - '.@$detalle_curso['descripcion_curso'])?></strong></h3><hr/>
<div class="pull-right">
	<a href="<?php echo site_url('admin/archivo/add/').@$detalle_curso['id_detalle_curso'].'/'.@$curso['id_curso']; ?>" class="btn btn-success">Agregar Archivos</a> 
</div>
<div class="pull-left">
	<a href="<?php echo site_url('admin/detalle_curso/index/').@$curso['id_curso'];?>" class="btn btn-danger">Volver</a> 
</div>
<table class="table table-striped table-bordered">
    <tr>
		<th>#</th>
		<th>Permitir Descarga</th>
		<!--
		<th>Tipo Archivo</th>
		-->
		<th>Uri Archivo</th>
		<th>Nombre Archivo</th>
		<th>Descripcion Archivo</th>
		<th>Actions</th>
    </tr>
	<?php foreach($archivos as $key => $a){ ?>
    <tr>
		<td><?php echo ++$key; ?></td>
		<td><?php echo ($a['descarga_archivo']==1?"Sí":"No"); ?></td>
		<!--
		<td><?php echo $a['tipo_archivo']; ?></td>
		-->
		<td><?php echo $a['uri_archivo']; ?></td>
		<td><?php echo $a['nombre_archivo']; ?></td>
		<td><?php echo $a['descripcion_archivo']; ?></td>
		<td>
            <a href="<?php echo site_url('admin/archivo/edit/'.$a['id_archivo']).'/'.$id_detalle_curso.'/'.$id_curso; ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/archivo/remove/'.$a['id_archivo']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
