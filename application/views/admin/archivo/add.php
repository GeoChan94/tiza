<h4>AGREGAR ARCHIVO AL CURSO <strong><?=mb_strtoupper(@$curso['nombre_curso'].' - '.@$detalle_curso['descripcion_curso'])?></strong></h4><hr/>
<?php echo form_open('admin/archivo/add/'.@$detalle_curso['id_detalle_curso'].'/'.@$curso['id_curso'],array("class"=>"form-horizontal", "id"=>"form-add-archivos")); ?>

	<div class="form-group">
		<label for="tipo_archivo" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Archivo</label>
		<div class="col-md-8">
			<select name="tipo_archivo" class="form-control">
				<option value="">select</option>
				<?php 
				$tipo_archivo_values = array(
					'0'=>'Seleccione...',
					'1'=>'Vídeo (MP4, AVI,FLV, ETC)',
					'2'=>'documento (PDF, DOCX, TXT,PPT,XLS,ETC )',
					'3'=>'imagen (JPEG,JPG,PNG, GIF,ETC)',
					'4'=>'Otros (RAR,ZIP,HTML,PHP,PSD,ETC)',
				);

				foreach($tipo_archivo_values as $value => $display_text)
				{
					$selected = ($value == $this->input->post('tipo_archivo')) ? ' selected="selected"' : "";

					echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_archivo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="nombre_archivo" class="col-md-4 control-label"><span class="text-danger">*</span>Nombre Archivo</label>
		<div class="col-md-8">
			<input type="text" name="nombre_archivo" value="<?php echo $this->input->post('nombre_archivo'); ?>" class="form-control" id="nombre_archivo" />
			<span class="text-danger"><?php echo form_error('nombre_archivo');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion_archivo" class="col-md-4 control-label">Descripción Archivo</label>
		<div class="col-md-8">
			<textarea name="descripcion_archivo" class="form-control" id="descripcion_archivo"><?php echo $this->input->post('descripcion_archivo'); ?></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="descarga_archivo" class="col-md-4 control-label">Descargar Archivo</label>
		<div class="col-md-8">
			<input type="checkbox" name="descarga_archivo" value="1" id="descarga_archivo" /> Permitir Descarga
		</div>
	</div>
	<!--
	<div class="form-group">
		<label for="uri_archivo" class="col-md-4 control-label"><span class="text-danger">*</span>Uri Archivo</label>
		<div class="col-md-8">
			<input type="text" name="uri_archivo" value="<?php echo $this->input->post('uri_archivo'); ?>" class="form-control" id="uri_archivo" />
			<span class="text-danger"><?php echo form_error('uri_archivo');?></span>
		</div>
	</div>
	-->
	<div class="form-group">
		<label for="descripcion_categoria" class="col-md-4 control-label">Seleccionar un archivo para el Curso</label>
		<div class="col-md-8">
			<div class="row">
				<div class="col-md-8">
					<div class="input-group">
						<div class="input-group-prepend">
						    <div class="btn btn-primary fileUpload">
					        	<span><i class="fa fa-upload" aria-hidden="true"></i> Upload</span>
					            <input id="archivos" name="archivos"  type="file" multiple="false" class="file archivos upload" value="Seleccionar archivo...">
					        </div> 
						</div>
						<input type="text" name="txt_archivo" value="<?php echo $this->input->post('uri_archivo'); ?>" class="form-control" id="txt_archivo" readonly placeholder="Seleccione un archivo"/>
					</div>
					<span class="text-danger"><?php echo form_error('uri_archivo');?></span>
				</div>
				<div class="col-md-4">
					<div class="imagencargada text-center" id="imagencargada"></div>
				</div>
			</div>
	    </div>
	</div><hr/>
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="<?=base_url()?>admin/archivo/index/<?=@$detalle_curso['id_detalle_curso'].'/'.@$curso['id_curso']?>" class="btn btn-danger">Volver</a>
        </div>
	</div>

<?php echo form_close(); ?>

<script>
jQuery(document).ready(function($) {
	    var fileExtension = "";
    $(':file').change(function(){
        var file = $("#archivos")[0].files[0];
        var fileName = file.name;
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        var fileSize = file.size;
        var fileType = file.type;
        //showMessage("<span class='info'>Archivo para subir: "+fileName+", "+fileSize+" bytes.</span>");
        var formData = new FormData($("#form-add-archivos")[0]);
        var message = ""; 
        $.ajax({
            url: '<?=base_url()?>assets/extra-file.php',  
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                $("#imagencargada").empty();
                $("#imagencargada").append('<img src="<?=base_url()?>assets/img/loading.gif" width="25%"/>');         
            },
            success: function(data){
                var rpta = $.parseJSON(data);
                //message = $("<span class='success'>La imagen ha subido correctamente.</span>");
                if(isImage(fileExtension)){
                    if ( rpta['rpta'] === 'success' ) {
                        $("#txt_archivo").val(rpta['imagen']);
                        $("#imagen").prop("readonly",true);                        
                    }
                    $("#imagencargada").empty().append('<i class="fa fa-check fa-2x text-success" title="Cargado con éxito"></i>'); 
                }
            },
            error: function(){
                 $("#imagencargada").empty().append('<i class="fa fa-times fa-2x text-danger" title="Error"></i>');
                //message = $("<span class='error'>Ha ocurrido un error.</span>");
                //showMessage(message);
            }
        });
    });
 
    function isImage(extension){
        return true;
        /*
        switch(extension.toLowerCase()) {
            case 'jpg': case 'gif': case 'png': case 'jpeg':
                return true;
            break;
            default:
                return false;
            break;
        }
        */
    }
});
</script>
<style type="text/css">
    .fileUpload {
    position: relative;
    overflow: hidden;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>