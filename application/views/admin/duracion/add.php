<h1>Agregar Duración</h1><hr/>
<?php echo form_open('admin/duracion/add/',array("class"=>"form-horizontal")); ?>
	<div class="form-group">
		<label for="nombre_duracion" class="col-md-6 control-label"><span class="text-danger">*</span>Nombre Duración (Ejemplo: Trimestral, Bimestre, Anual, etc)</label>
		<div class="col-md-8">
			<input type="text" name="nombre_duracion" value="<?php echo $this->input->post('nombre_duracion'); ?>" class="form-control" id="nombre_duracion" autofocus/>
			<span class="text-danger"><?php echo form_error('nombre_duracion');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="cantidad_duracion" class="col-md-6 control-label"><span class="text-danger">*</span>Cantidad Duración (Ejemplo: 4 = 4 semanas, días, meses, etc)</label>
		<div class="col-md-8">
			<input type="text" name="cantidad_duracion" value="<?php echo $this->input->post('cantidad_duracion'); ?>" class="form-control" id="cantidad_duracion" />
			<span class="text-danger"><?php echo form_error('cantidad_duracion');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="tipo_duracion" class="col-md-4 control-label"><span class="text-danger">*</span>Tipo Duracion</label>
		<div class="col-md-8">
			<select name="tipo_duracion" class="form-control">
				<option value="">select</option>
				<?php 
				$tipo_duracion_values = array(
					'0'=>'Seleccione...',
					'1'=>'Minutos',
					'2'=>'Horas',
					'3'=>'Días',
					'4'=>'Semanas',
					'5'=>'Meses',
					'6'=>'Años',
					'7'=>'Semestres',
					'8'=>'Bimestres',
					'9'=>'Trimestres',
				);

				foreach($tipo_duracion_values as $value => $display_text)
				{
					$selected = ($value == $this->input->post('tipo_duracion')) ? ' selected="selected"' : "";

					echo '<option value="'.$value.'" '.$selected.'>'.$display_text.'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('tipo_duracion');?></span>
		</div>
	</div>
	
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="<?=base_url()?>admin/duracion/" class="btn btn-danger">Volver</a>
        </div>
	</div>

<?php echo form_close(); ?>