<div class="pull-right">
	<a href="<?php echo site_url('admin/duracion/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>#</th>
		<th>Duración</th>
		<th>Actions</th>
    </tr>
    <?php
    	$tipo_duracion = array(
			'1'=>'Minutos',
			'2'=>'Horas',
			'3'=>'Días',
			'4'=>'Semanas',
			'5'=>'Meses',
			'6'=>'Años',
			'7'=>'Semestres',
			'8'=>'Bimestres',
			'9'=>'Trimestres',
		);
    ?>
	<?php foreach($duraciones as $key => $d){ ?>
    <tr>
		<td><?php echo ++$key; ?></td>
		<!--
		<td><?php echo $d['cantidad_duracion'].' '.$tipo_duracion[$d['tipo_duracion']]; ?></td>
		-->
		<td><?php echo $d['nombre_duracion']?></td>
		<td>
            <a href="<?php echo site_url('admin/duracion/edit/'.$d['id_duracion']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('admin/duracion/remove/'.$d['id_duracion']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
