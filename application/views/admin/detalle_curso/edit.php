<?php echo form_open('admin/detalle_curso/edit/'.$detalle_curso['id_detalle_curso'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_curso" class="col-md-4 control-label"><span class="text-danger">*</span>Curso</label>
		<div class="col-md-8">
			<select name="id_curso" class="form-control">
				<option value="">select curso</option>
				<?php 
				foreach($all_cursos as $curso)
				{
					$selected = ($curso['id_curso'] == $detalle_curso['id_curso']) ? ' selected="selected"' : "";

					echo '<option value="'.$curso['id_curso'].'" '.$selected.'>'.$curso['nombre_curso'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('id_curso');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion_curso" class="col-md-4 control-label"><span class="text-danger">*</span>Descripcion Curso</label>
		<div class="col-md-8">
			<textarea name="descripcion_curso" class="form-control" id="descripcion_curso"><?php echo ($this->input->post('descripcion_curso') ? $this->input->post('descripcion_curso') : $detalle_curso['descripcion_curso']); ?></textarea>
			<span class="text-danger"><?php echo form_error('descripcion_curso');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="id_curso" class="col-md-4 control-label">Seleccione duración del curso<span class="text-danger">*</span></label>
		<div class="col-md-8">
			<select name="id_duracion" class="form-control" <?=($duraciones>0?'readonly':'')?> >
				<option value="">seleccione</option>
				<?php 
				foreach($duraciones as $duracion){
					$selected = ($duracion['id_duracion'] == $this->input->post('id_duracion') || $detalle_curso['id_duracion'] == $duracion['id_duracion'] ) ? ' selected="selected"' : "";

					echo '<option value="'.$duracion['id_duracion'].'" '.$selected.'>'.$duracion['cantidad_duracion'].' '.$duracion['nombre_duracion'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('id_duracion');?></span>
		</div>
	</div>	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="<?=base_url()?>admin/detalle_curso/index/<?=$detalle_curso['id_curso']?>" class="btn btn-danger">Volver</a>
        </div>
	</div>
	
<?php echo form_close(); ?>