<h3 class="text-center"> CURSO <?=mb_strtoupper($curso['nombre_curso'])?></h3><hr/>
<div class="pull-right">
	<a href="<?php echo site_url('admin/detalle_curso/add/').@$curso['id_curso']; ?>" class="btn btn-success">Agregar Detalle Curso</a>
</div>
<div class="pull-left">
	<a href="<?=base_url()?>admin/curso/" class="btn btn-danger">Volver a Cursos</a> 
</div>
<?php
//echo json_encode($detalles_curso);
?>
<table class="table table-striped table-bordered">
    <tr>
		<th>#</th>
		<th>Curso</th>
		<th>Detalles Curso</th>
		<th>Actions</th>
    </tr>
	<?php foreach($detalles_curso as $key => $d){ ?>
    <tr>
		<td><?php echo ++$key; ?></td>
		<td><?php echo $d['nombre_curso']; ?></td>
		<td><?php echo $d['descripcion_curso']; ?></td>
		<td>
			<div class="btn-group">
            	<a href="<?php echo site_url('admin/detalle_curso/edit/'.$d['id_detalle_curso']); ?>" class="btn btn-info btn-xs" title="Editar"><i class="fa fa-pencil"></i></a> 
            	<a href="<?php echo site_url('admin/archivo/index/'.$d['id_detalle_curso'].'/'.$d['id_curso']); ?>" class="btn btn-success btn-xs" title="Archivos">(<?=@$d['archivo']['cantidad']?>) <i class="fa fa-files-o"></i></a>
            	<a href="<?php echo site_url('admin/horario/index/'.$d['id_detalle_curso'].'/'.$d['id_curso']); ?>" class="btn btn-primary btn-xs" title="Horarios">(<?=@$d['horario']['cantidad']?>) <i class="fa fa-clock-o"></i></a>
            	<a href="<?php echo site_url('admin/docente/index/'.$d['id_detalle_curso'].'/'.$d['id_curso']); ?>" class="btn btn-warning btn-xs" title="Docentes">(<?=@$d['docente']['cantidad']?>) <i class="fa fa-user"></i></a>
            	<a href="javascript:void(0)" data-id="<?=$d['id_detalle_curso']?>" class="btn btn-danger btn-xs btn-eliminar" title="Eliminar"><i class="fa fa-remove"></i></a>
        	</div>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.btn-eliminar', function(event) {
			event.preventDefault();
			var id = $(this).data('id');
			console.log(id);
		});
	});
</script>