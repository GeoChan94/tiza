<?php echo form_open('admin/curso/edit/'.$curso['id_curso'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_categoria" class="col-md-4 control-label">Categoria<span class="text-danger">*</span></label>

		<div class="col-md-8">
			<select name="id_categoria" class="form-control">
				<option value="">select categoria</option>
				<?php 
				foreach($all_categorias as $categoria)
				{
					$selected = ($categoria['id_categoria'] == $curso['id_categoria']) ? ' selected="selected"' : "";

					echo '<option value="'.$categoria['id_categoria'].'" '.$selected.'>'.$categoria['nombre_categoria'].'</option>';
				} 
				?>
			</select>
			<span class="text-danger"><?php echo form_error('id_categoria');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="url_curso" class="col-md-4 control-label">Url Curso<span class="text-danger">*</span></label>
		<div class="col-md-8">
			<input type="text" name="url_curso" value="<?php echo ($this->input->post('url_curso') ? $this->input->post('url_curso') : $curso['url_curso']); ?>" class="form-control" id="url_curso" />
			<span class="text-danger"><?php echo form_error('url_curso');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="nombre_curso" class="col-md-4 control-label">Nombre Curso<span class="text-danger">*</span></label>
		<div class="col-md-8">
			<input type="text" name="nombre_curso" value="<?php echo ($this->input->post('nombre_curso') ? $this->input->post('nombre_curso') : $curso['nombre_curso']); ?>" class="form-control" id="nombre_curso" />
			<span class="text-danger"><?php echo form_error('nombre_curso');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="descripcion_curso" class="col-md-4 control-label">Descripcion Curso</label>
		<div class="col-md-8">
			<textarea name="descripcion_curso" class="form-control" id="descripcion_curso" /><?php echo ($this->input->post('descripcion_curso') ? $this->input->post('descripcion_curso') : $curso['descripcion_curso']); ?></textarea>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Guardar</button>
			<a href="<?=base_url()?>admin/curso" class="btn btn-danger">Volver</a>
        </div>
	</div>
	
<?php echo form_close(); ?>