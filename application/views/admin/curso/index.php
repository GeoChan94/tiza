<div class="pull-right">
	<a href="<?php echo site_url('admin/curso/add'); ?>" class="btn btn-success">Agregar Curso</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>#</th>
		<th>Categoria</th>
		<th>Curso</th>
		<th>Descripción</th>
		<th>Actions</th>
    </tr>
	<?php foreach($cursos as $key => $c){ ?>
    <tr>
		<td><?php echo ++$key; ?></td>
		<td><?php echo $c['nombre_categoria']; ?></td>
		<td><?php echo $c['nombre_curso']; ?></td>
		<td><?php echo $c['descripcion_curso']; ?></td>
		<td>
			<div class="btn-group">
	            <a href="<?php echo site_url('admin/curso/edit/'.$c['id_curso']); ?>" class="btn btn-info btn-xs" title="Editar Curso"><i class="fa fa-pencil"></i></a> 
	            <a href="<?php echo site_url('admin/detalle_curso/index/'.$c['id_curso']); ?>" class="btn btn-warning btn-xs" title="Detalles del Curso"><i class="fa fa-th-list"></i></a> 
	            <!--
	            <a href="<?php echo site_url('admin/detalle_curso/add/'.$c['id_curso']); ?>" class="btn btn-warning btn-xs" title="Agregar detalles al Curso"><i class="fa fa-th-list"></i></a> 
	            <a href="<?php echo site_url('admin/horario/add/'.$c['id_curso']); ?>" class="btn btn-info btn-xs" title="Horarios">(<?=@$c['horario']['cantidad']?>) <i class="fa fa-clock-o"></i></a>
	            <a href="<?php echo site_url('admin/duracion/add/'.$c['id_curso']); ?>" class="btn btn-warning btn-xs" title="Duraciones">(<?=@$c['duracion']['cantidad']?>) <i class="fa fa-calendar"></i></a> 
	        	<a href="<?php echo site_url('admin/archivo/add/'.$c['id_curso']); ?>" class="btn btn-success btn-xs" title="Archivos">(<?=@$c['archivo']['cantidad']?>) <i class="fa fa-files-o"></i></a>
	            -->
	            <a href="<?php echo site_url('admin/docente/index/'.$c['id_curso']); ?>" class="btn btn-primary btn-xs" title="Docentes">(<?=@$c['docente']['cantidad']?>) <i class="fa fa-user"></i></a> 

	            <a href="javascript:void(0)" data-id="<?=$c['id_curso']?>" class="btn btn-danger btn-xs btn-eliminar"><i class="fa fa-remove"></i></a>
	        </div>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(document).on('click', '.btn-eliminar', function(event) {
			event.preventDefault();
			var id = $(this).data('id');
			console.log(id);
		});
	});
</script>
