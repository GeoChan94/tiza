<?php echo form_open('administrador/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_usuario" class="col-md-4 control-label">Usuario</label>
		<div class="col-md-8">
			<select name="id_usuario" class="form-control">
				<option value="">select usuario</option>
				<?php 
				foreach($all_usuarios as $usuario)
				{
					$selected = ($usuario['id_usuario'] == $this->input->post('id_usuario')) ? ' selected="selected"' : "";

					echo '<option value="'.$usuario['id_usuario'].'" '.$selected.'>'.$usuario['id_usuario'].'</option>';
				} 
				?>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label for="nombres_administrador" class="col-md-4 control-label"><span class="text-danger">*</span>Nombres Administrador</label>
		<div class="col-md-8">
			<input type="text" name="nombres_administrador" value="<?php echo $this->input->post('nombres_administrador'); ?>" class="form-control" id="nombres_administrador" />
			<span class="text-danger"><?php echo form_error('nombres_administrador');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="apellidos_administrador" class="col-md-4 control-label"><span class="text-danger">*</span>Apellidos Administrador</label>
		<div class="col-md-8">
			<input type="text" name="apellidos_administrador" value="<?php echo $this->input->post('apellidos_administrador'); ?>" class="form-control" id="apellidos_administrador" />
			<span class="text-danger"><?php echo form_error('apellidos_administrador');?></span>
		</div>
	</div>
	<div class="form-group">
		<label for="email_administrador" class="col-md-4 control-label"><span class="text-danger">*</span>Email Administrador</label>
		<div class="col-md-8">
			<input type="text" name="email_administrador" value="<?php echo $this->input->post('email_administrador'); ?>" class="form-control" id="email_administrador" />
			<span class="text-danger"><?php echo form_error('email_administrador');?></span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>