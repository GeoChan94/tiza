<div class="pull-right">
	<a href="<?php echo site_url('administrador/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Administrador</th>
		<th>Id Usuario</th>
		<th>Nombres Administrador</th>
		<th>Apellidos Administrador</th>
		<th>Email Administrador</th>
		<th>Actions</th>
    </tr>
	<?php foreach($administradores as $a){ ?>
    <tr>
		<td><?php echo $a['id_administrador']; ?></td>
		<td><?php echo $a['id_usuario']; ?></td>
		<td><?php echo $a['nombres_administrador']; ?></td>
		<td><?php echo $a['apellidos_administrador']; ?></td>
		<td><?php echo $a['email_administrador']; ?></td>
		<td>
            <a href="<?php echo site_url('administrador/edit/'.$a['id_administrador']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('administrador/remove/'.$a['id_administrador']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
<div class="pull-right">
    <?php echo $this->pagination->create_links(); ?>    
</div>
