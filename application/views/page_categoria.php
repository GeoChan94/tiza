<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title></title>
    <?php
        $this->load->view('recursos/css');
    ?>
</head>
<body class="bg-light">
    <?php
        $this->load->view('menu/header');
        $this->load->view('menu/menu');
    ?> 
    <content data-spy="scroll" data-target="#main-menu" data-offset="0" >
    <?php
        $this->load->view('page/page_categoria');
    ?>
</content>  
<?php
    $this->load->view('recursos/js');
?> 
<?php
    $this->load->view('footer/footer');
?>    
</body>

</html>