<?php

class Curso_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->model('Horario_model');
        $this->load->model('Duracion_model');
        $this->load->model('Archivo_model');
        $this->load->model('Docente_model');

    }
    
    function get_curso_uri($uri){
        $curso = $this->db->query("SELECT * FROM categoria AS cat JOIN  curso AS cur ON cat.id_categoria = cur.id_categoria AND cur.url_curso = ?;",array($uri))->row_array();
        return $curso;
    }
    function get_curso($id){
        $curso = $this->db->query("SELECT * FROM categoria AS cat JOIN  curso AS cur ON cat.id_categoria = cur.id_categoria AND cur.id_curso = ?;",array($id))->row_array();
        return $curso;
    }
    

    function get_all_cursos_count(){
        $cursos = $this->db->query("SELECT count(*) as count FROM curso;")->row_array();
        return $cursos['count'];
    }
        
    function get_all_cursos($params = array()){
        $limit_condition = "";
        if(isset($params) && !empty($params)){
            $limit_condition = " LIMIT " . $params['offset'] . "," . $params['limit'];
        }
        $data = [];;
        $cursos = $this->db->query("SELECT * FROM categoria AS cat JOIN  curso AS cur ON cat.id_categoria = cur.id_categoria AND 1 = 1 ORDER BY cur.id_curso DESC " . $limit_condition . ";")->result_array();
        if ( !empty($cursos) ) {
            foreach ($cursos as $key => $value ) {
                $value['horario'] = $this->Horario_model->get_count_horario_curso($value['id_curso']);
                //$value['duracion'] = $this->Duracion_model->get_count_duracion_curso($value['id_curso']);
                $value['archivo'] = $this->Archivo_model->get_count_archivo_detalle_curso($value['id_curso']);
                $value['docente'] = $this->Docente_model->get_count_docente_curso($value['id_curso']);

                $data[] = $value;
            }
        }
        return $data;
        //return $cursos;
    }
        
    function add_curso($params){
        $this->db->insert('curso',$params);
        return $this->db->insert_id();
    }
    
    function update_curso($id_curso,$params){
        $this->db->where('id_curso',$id_curso);
        return $this->db->update('curso',$params);
    }
    
    function delete_curso($id_curso){
        return $this->db->delete('curso',array('id_curso'=>$id_curso));
    }
}
