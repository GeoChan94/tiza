<?php

class Docente_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_docente($id_docente){
        $docente = $this->db->query("SELECT * FROM docente WHERE id_docente = ?;",array($id_docente))->row_array();
        return $docente;
    }
    
    function get_all_docentes_count(){
        $docentes = $this->db->query("SELECT count(*) as count FROM docente;")->row_array();
        return $docentes['count'];
    }

    function get_all_docentes($params = array()){
        $limit_condition = "";
        if(isset($params) && !empty($params))
            $limit_condition = " LIMIT " . $params['offset'] . "," . $params['limit'];
        
        $docentes = $this->db->query("SELECT * FROM docente WHERE 1 = 1 ORDER BY id_docente DESC " . $limit_condition . ";")->result_array();

        return $docentes;
    }
        
    function add_docente($params){
        $this->db->insert('docente',$params);
        return $this->db->insert_id();
    }
    
    function update_docente($id_docente,$params){
        $this->db->where('id_docente',$id_docente);
        return $this->db->update('docente',$params);
    }
    
    function delete_docente($id_docente){
        return $this->db->delete('docente',array('id_docente'=>$id_docente));
    }

    function get_count_docente_curso( $id_curso = null ){
        return $this->db->query('SELECT COUNT(*) as cantidad FROM curso_has_docente AS chd JOIN curso AS cur ON chd.id_curso = cur.id_curso AND chd.id_curso = ?',array($id_curso))->row_array();
    }

    function get_docentesByDetalleCurso($id_curso = null, $id_detalle_curso = null ){
        return $this->db->query( 'SELECT doce.* FROM curso AS curs JOIN detalle_curso AS decu ON decu.id_curso = decu.id_curso AND decu.id_detalle_curso = ? JOIN detalle_curso_has_docente AS dcdo ON decu.id_detalle_curso = dcdo.id_detalle_curso JOIN docente AS doce ON doce.id_docente = doce.id_docente;', array($id_detalle_curso) )->result_array();
    }
}
