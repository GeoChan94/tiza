<?php

class Duracion_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_duracion($id_duracion){
        $duracion = $this->db->query("
            SELECT
                *

            FROM
                `duracion`

            WHERE
                `id_duracion` = ?
        ",array($id_duracion))->row_array();

        return $duracion;
    }
    
    function get_all_duraciones_count(){
        $duraciones = $this->db->query("
            SELECT
                count(*) as count

            FROM
                `duracion`
        ")->row_array();

        return $duraciones['count'];
    }
        
    function get_all_duraciones($params = array()){
        $limit_condition = "";
        if(isset($params) && !empty($params))
            $limit_condition = " LIMIT " . $params['offset'] . "," . $params['limit'];
        
        $duraciones = $this->db->query("
            SELECT
                *

            FROM
                `duracion`

            WHERE
                1 = 1

            ORDER BY `id_duracion` DESC

            " . $limit_condition . "
        ")->result_array();

        return $duraciones;
    }

    function add_duracion($params)
    {
        $this->db->insert('duracion',$params);
        return $this->db->insert_id();
    }
    
    function update_duracion($id_duracion,$params){
        $this->db->where('id_duracion',$id_duracion);
        return $this->db->update('duracion',$params);
    }
    
    function delete_duracion($id_duracion){
        return $this->db->delete('duracion',array('id_duracion'=>$id_duracion));
    }

    function get_count_duracion_curso( $id_curso = null ){
        return $this->db->query('SELECT COUNT(*) as cantidad FROM duracion WHERE id_curso = ?',array($id_curso))->row_array();
    }
}
