<?php

class Detalle_curso_has_horario_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_detalle_curso_has_horario($id_detalle_curso){
        $detalle_curso_has_horario = $this->db->query("SELECT * FROM detalle_curso_has_horario WHERE id_detalle_curso = ?",array($id_detalle_curso))->row_array();
        return $detalle_curso_has_horario;
    }
        
    function get_all_cursos_has_horario(){
        $cursos_has_horario = $this->db->query("
            SELECT
                *

            FROM
                `detalle_curso_has_horario`

            WHERE
                1 = 1

            ORDER BY `id_detalle_curso` DESC
        ")->result_array();

        return $cursos_has_horario;
    }
    
    function add_detalle_curso_has_horario($params){
        $this->db->insert('detalle_curso_has_horario',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update detalle_curso_has_horario
     */
    function update_detalle_curso_has_horario($id_detalle_curso,$params){
        $this->db->where('id_detalle_curso',$id_detalle_curso);
        return $this->db->update('detalle_curso_has_horario',$params);
    }
    
    /*
     * function to delete detalle_curso_has_horario
     */
    function delete_detalle_curso_has_horario($id_detalle_curso)
    {
        return $this->db->delete('detalle_curso_has_horario',array('id_detalle_curso'=>$id_detalle_curso));
    }
}
