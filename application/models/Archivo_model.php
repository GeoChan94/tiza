<?php

class Archivo_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_archivo($id_archivo){
        $archivo = $this->db->query("SELECT * FROM archivo WHERE id_archivo = ?",array($id_archivo))->row_array();
        return $archivo;
    }
    
    function get_all_archivos_count(){
        $archivos = $this->db->query("SELECT count(*) as count FROM archivo;")->row_array();
        return $archivos['count'];
    }

    function get_all_archivos($params = array()){
        $limit_condition = "";
        if(isset($params) && !empty($params))
            $limit_condition = " LIMIT " . $params['offset'] . "," . $params['limit'];
        
        $archivos = $this->db->query("SELECT * FROM archivo WHERE 1 = 1 ORDER BY id_archivo DESC " . $limit_condition . ";")->result_array();
        return $archivos;
    }
        
    function add_archivo($params){
        $this->db->insert('archivo',$params);
        return $this->db->insert_id();
    }
    
    function update_archivo($id_archivo,$params){
        $this->db->where('id_archivo',$id_archivo);
        return $this->db->update('archivo',$params);
    }
    
    function delete_archivo($id_archivo){
        return $this->db->delete('archivo',array('id_archivo'=>$id_archivo));
    }

    function get_count_archivo_detalle_curso( $id_detalle_curso = null ){
        return $this->db->query('SELECT COUNT(*) as cantidad FROM curso AS cur JOIN detalle_curso AS decu ON cur.id_curso = decu.id_curso JOIN detalle_curso_has_archivo AS dcha ON decu.id_detalle_curso = dcha.id_detalle_curso JOIN archivo AS arc ON dcha.id_archivo = arc.id_archivo AND decu.id_detalle_curso = ?',array($id_detalle_curso))->row_array();
    }

    function get_archivosByDetalleCurso( $id_curso = null, $id_detalle_curso = null ){
        return $this->db->query('SELECT arch.* FROM curso AS curs JOIN detalle_curso AS decu ON decu.id_curso = decu.id_curso AND decu.id_detalle_curso = ? JOIN detalle_curso_has_archivo AS dcar ON decu.id_detalle_curso = dcar.id_detalle_curso JOIN archivo AS arch ON arch.id_archivo = dcar.id_archivo ORDER BY arch.tipo_archivo ASC;', array($id_detalle_curso) )->result_array();
    }
}
