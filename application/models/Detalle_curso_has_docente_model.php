<?php

class Detalle_curso_has_docente_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_detalle_curso_has_docente($id_detalle_curso){
        $detalle_curso_has_docente = $this->db->query("
            SELECT
                *

            FROM
                `detalle_curso_has_docente`

            WHERE
                `id_detalle_curso` = ?
        ",array($id_detalle_curso))->row_array();

        return $detalle_curso_has_docente;
    }
        
    function get_all_cursos_has_docente(){
        $cursos_has_docente = $this->db->query("
            SELECT
                *

            FROM
                `detalle_curso_has_docente`

            WHERE
                1 = 1

            ORDER BY `id_detalle_curso` DESC
        ")->result_array();

        return $cursos_has_docente;
    }

    function add_detalle_curso_has_docente($params){
        $this->db->insert('detalle_curso_has_docente',$params);
        return $this->db->insert_id();
    }
    
    function update_detalle_curso_has_docente($id_detalle_curso,$params){
        $this->db->where('id_detalle_curso',$id_detalle_curso);
        return $this->db->update('detalle_curso_has_docente',$params);
    }
    
    function delete_detalle_curso_has_docente($id_detalle_curso){
        return $this->db->delete('detalle_curso_has_docente',array('id_detalle_curso'=>$id_detalle_curso));
    }
}
