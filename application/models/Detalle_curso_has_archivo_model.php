<?php

class Detalle_curso_has_archivo_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_detalle_curso_has_archivo($id_detalle_curso){
        $detalle_curso_has_archivo = $this->db->query("SELECT * FROM detalle_curso_has_archivo WHERE id_detalle_curso = ?",array($id_detalle_curso))->row_array();

        return $detalle_curso_has_archivo;
    }

    function get_all_detalles_curso_has_archivo(){
        $detalles_curso_has_archivo = $this->db->query("SELECT * FROM `detalle_curso_has_archivo` WHERE 1 = 1 ORDER BY `id_detalle_curso` DESC")->result_array();

        return $detalles_curso_has_archivo;
    }
        
    function add_detalle_curso_has_archivo($params){
        $this->db->insert('detalle_curso_has_archivo',$params);
        return $this->db->insert_id();
    }
    
    function update_detalle_curso_has_archivo($id_detalle_curso,$params)
    {
        $this->db->where('id_detalle_curso',$id_detalle_curso);
        return $this->db->update('detalle_curso_has_archivo',$params);
    }
    
    /*
     * function to delete detalle_curso_has_archivo
     */
    function delete_detalle_curso_has_archivo($id_detalle_curso)
    {
        return $this->db->delete('detalle_curso_has_archivo',array('id_detalle_curso'=>$id_detalle_curso));
    }
}
