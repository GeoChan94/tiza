<?php

class Administrador_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_administrador($id_administrador){
        $administrador = $this->db->query("SELECT * FROM administrador WHERE id_administrador = ?",array($id_administrador))->row_array();
        return $administrador;
    }
    
    function get_all_administradores_count(){
        $administradores = $this->db->query("SELECT count(*) as count FROM administrador")->row_array();
        return $administradores['count'];
    }

    function get_all_administradores($params = array()){
        $limit_condition = "";
        if(isset($params) && !empty($params))
            $limit_condition = " LIMIT " . $params['offset'] . "," . $params['limit'];
        
        $administradores = $this->db->query(" SELECT * FROM administrador WHERE 1 = 1 ORDER BY id_administrador DESC " . $limit_condition . "")->result_array();
        return $administradores;
    }

    function add_administrador($params){
        $this->db->insert('administrador',$params);
        return $this->db->insert_id();
    }
    
    function update_administrador($id_administrador,$params){
        $this->db->where('id_administrador',$id_administrador);
        return $this->db->update('administrador',$params);
    }
    
    function delete_administrador($id_administrador){
        return $this->db->delete('administrador',array('id_administrador'=>$id_administrador));
    }

    function get_login( $user = null, $password = null ){
        return $this->db->query("SELECT * FROM usuario AS usua JOIN administrador AS admi ON usua.id_usuario = admi.id_usuario AND usua.usuario = ? OR admi.email_administrador = ? AND usua.password = ?;",array($user,$user,$password));
    }
}
