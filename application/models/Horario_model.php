<?php

class Horario_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    function get_horario($id_horario){
        $horario = $this->db->query("SELECT * FROM horario WHERE id_horario = ?; ",array($id_horario))->row_array();
        return $horario;
    }
    
    function get_all_horarios_count(){
        $horarios = $this->db->query("SELECT count(*) as count FROM horario;")->row_array();
        return $horarios['count'];
    }
        
    function get_all_horarios($params = array()){
        $limit_condition = "";
        if(isset($params) && !empty($params))
            $limit_condition = " LIMIT " . $params['offset'] . "," . $params['limit'];
        
        $horarios = $this->db->query("SELECT * FROM `horario` WHERE 1 = 1 ORDER BY `id_horario` DESC " . $limit_condition . ";")->result_array();

        return $horarios;
    }
        
    function add_horario($params){
        $this->db->insert('horario',$params);
        return $this->db->insert_id();
    }
    
    function update_horario($id_horario,$params){
        $this->db->where('id_horario',$id_horario);
        return $this->db->update('horario',$params);
    }
    
    function delete_horario($id_horario){
        return $this->db->delete('horario',array('id_horario'=>$id_horario));
    }

    function get_count_horario_curso( $id_curso = null ){
        return $this->db->query('SELECT COUNT(*) as cantidad FROM curso_has_horario AS chh JOIN horario AS hor ON chh.id_horario = hor.id_horario AND chh.id_curso = ?',array($id_curso))->row_array();
    }

    function get_horariosByDetalleCurso($id_curso = null, $id_detalle_curso = null ){
        return $this->db->query( 'SELECT hora.* FROM curso AS curs JOIN detalle_curso AS decu ON decu.id_curso = decu.id_curso AND decu.id_detalle_curso = ? JOIN detalle_curso_has_horario AS dcho ON decu.id_detalle_curso = dcho.id_detalle_curso JOIN horario AS hora ON hora.id_horario = dcho.id_horario;', array($id_detalle_curso) )->result_array();
    }
}
