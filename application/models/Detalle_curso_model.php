<?php

class Detalle_curso_model extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->model('Archivo_model');
        $this->load->model('Curso_model');
    }
    
    function get_detalle_curso($id_detalle_curso){
        $detalle_curso = $this->db->query("SELECT * FROM curso AS cur JOIN detalle_curso AS decu ON cur.id_curso = decu.id_curso AND decu.id_detalle_curso= ? JOIN duracion AS dura ON dura.id_duracion = decu.id_duracion;",array($id_detalle_curso))->row_array();
        return $detalle_curso;
    }
    function get_detalle_cursos($id_curso){
        // $this->db->order_by("id_detalle_curso", "desc");
        $detalle_cursos = $this->db->query("SELECT * FROM curso AS cur JOIN detalle_curso AS decu ON cur.id_curso = decu.id_curso AND cur.id_curso= ?",array($id_curso))->result_array();

        return $detalle_cursos;
    }
    
    function get_all_detalles_curso_count(){
        $detalles_curso = $this->db->query("SELECT count(*) as count FROM detalle_curso;")->row_array();
        return $detalles_curso['count'];
    }
        
    function get_all_detalles_curso($params = array()){
        $limit_condition = "";
        if(isset($params) && !empty($params))
            $limit_condition = " LIMIT " . $params['offset'] . "," . $params['limit'];
        
        $detalles_curso = $this->db->query("SELECT * FROM curso AS cur JOIN detalle_curso AS decu ON cur.id_curso = decu.id_curso ORDER BY decu.id_detalle_curso DESC " . $limit_condition .";")->result_array();

         $data = [];;
        if ( !empty($detalles_curso) ) {
            foreach ($detalles_curso as $key => $value ) {
                $value['horario'] = $this->Horario_model->get_count_horario_curso($value['id_curso']);
                //$value['duracion'] = $this->Duracion_model->get_count_duracion_curso($value['id_curso']);
                $value['archivo'] = $this->Archivo_model->get_count_archivo_detalle_curso($value['id_detalle_curso']);
                $value['docente'] = $this->Docente_model->get_count_docente_curso($value['id_curso']);

                $data[] = $value;
            }
        }
        return $data;

        //return $detalles_curso;
    }
        
    function add_detalle_curso($params){
        $this->db->insert('detalle_curso',$params);
        return $this->db->insert_id();
    }
    
    function update_detalle_curso($id_detalle_curso,$params){
        $this->db->where('id_detalle_curso',$id_detalle_curso);
        return $this->db->update('detalle_curso',$params);
    }
    
    function delete_detalle_curso($id_detalle_curso){
        return $this->db->delete('detalle_curso',array('id_detalle_curso'=>$id_detalle_curso));
    }
}
