<?php

class Horario extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Horario_model');
        $this->load->model('Curso_model');
        $this->load->model('Detalle_curso_model');
        $this->load->model('Detalle_curso_has_horario_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index( $id_detalle_curso = null, $id_curso = null ){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/horario/index?');
        $config['total_rows'] = $this->Horario_model->get_all_horarios_count();
        $this->pagination->initialize($config);

        $data['curso'] = $this->Curso_model->get_curso($id_curso);
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        $data['id_detalle_curso'] = $id_detalle_curso;
        $data['id_curso'] = $id_curso;

        $data['horarios'] = $this->Horario_model->get_all_horarios($params);
        
        $data['_view'] = 'admin/horario/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add( $id_detalle_curso = null , $id_curso = null ){   
        $data['curso'] = $this->Curso_model->get_curso(trim($id_curso));
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        $data['id_detalle_curso'] = $id_detalle_curso;
        $data['id_curso'] = $id_curso;

        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

		$this->form_validation->set_rules('nombre_horario','Nombre Horario','required');
		$this->form_validation->set_rules('cantidad_horario','Cantidad Horario','required|numeric');
		$this->form_validation->set_rules('tipo_horario','Tipo Horario','required');
		
		if($this->form_validation->run()){   
            $params = array(
				'tipo_horario' => $this->input->post('tipo_horario'),
				'nombre_horario' => $this->input->post('nombre_horario'),
				'cantidad_horario' => $this->input->post('cantidad_horario'),
            );
            
            $horario_id = $this->Horario_model->add_horario($params);
            $this->Detalle_curso_has_horario_model->add_detalle_curso_has_horario(
                array(
                    "id_detalle_curso" => trim($id_detalle_curso),
                    "id_horario" => trim($horario_id),
                )
            );
            //redirect('admin/horario/index/'.$horario_id);
            redirect('admin/horario/index/'.$id_detalle_curso.'/'.$id_curso);
        }else{            
            $data['_view'] = 'admin/horario/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_horario,$id_detalle_curso = null , $id_curso = null){   
        // check if the horario exists before trying to edit it
        $data['horario'] = $this->Horario_model->get_horario($id_horario);
        $data['curso'] = $this->Curso_model->get_curso(trim($id_curso));
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        $data['id_detalle_curso'] = $id_detalle_curso;
        $data['id_curso'] = $id_curso;
        
        if(isset($data['horario']['id_horario'])){
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

			$this->form_validation->set_rules('nombre_horario','Nombre Horario','required');
			$this->form_validation->set_rules('cantidad_horario','Cantidad Horario','required|numeric');
			$this->form_validation->set_rules('tipo_horario','Tipo Horario','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'tipo_horario' => $this->input->post('tipo_horario'),
					'nombre_horario' => $this->input->post('nombre_horario'),
					'cantidad_horario' => $this->input->post('cantidad_horario'),
                );

                $this->Horario_model->update_horario($id_horario,$params);            
                redirect('admin/horario/index/'.$id_detalle_curso.'/'.$id_curso);
            }else{
                $data['_view'] = 'admin/horario/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The horario you are trying to edit does not exist.');
        }
    } 

    function remove($id_horario){
        $horario = $this->Horario_model->get_horario($id_horario);

        // check if the horario exists before trying to delete it
        if(isset($horario['id_horario'])){
            $this->Horario_model->delete_horario($id_horario);
            redirect('admin/horario/index');
        }else{
            show_error('The horario you are trying to delete does not exist.');
        }
    }
    
}
