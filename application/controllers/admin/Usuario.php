<?php

class Usuario extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Usuario_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index(){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/usuario/index?');
        $config['total_rows'] = $this->Usuario_model->get_all_usuarios_count();
        $this->pagination->initialize($config);

        $data['usuarios'] = $this->Usuario_model->get_all_usuarios($params);
        
        $data['_view'] = 'admin/usuario/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add(){   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('usuario','Usuario','required');
		
		if($this->form_validation->run()){   
            $params = array(
				'password' => $this->input->post('password'),
				'usuario' => $this->input->post('usuario'),
            );
            
            $usuario_id = $this->Usuario_model->add_usuario($params);
            redirect('admin/usuario/index');
        }else{            
            $data['_view'] = 'admin/usuario/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_usuario){   
        // check if the usuario exists before trying to edit it
        $data['usuario'] = $this->Usuario_model->get_usuario($id_usuario);
        
        if(isset($data['usuario']['id_usuario'])){
            $this->load->library('form_validation');

			$this->form_validation->set_rules('password','Password','required');
			$this->form_validation->set_rules('usuario','Usuario','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'password' => $this->input->post('password'),
					'usuario' => $this->input->post('usuario'),
                );

                $this->Usuario_model->update_usuario($id_usuario,$params);            
                redirect('admin/usuario/index');
            }else{
                $data['_view'] = 'admin/usuario/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The usuario you are trying to edit does not exist.');
        }
    } 

    function remove($id_usuario){
        $usuario = $this->Usuario_model->get_usuario($id_usuario);

        // check if the usuario exists before trying to delete it
        if(isset($usuario['id_usuario'])){
            $this->Usuario_model->delete_usuario($id_usuario);
            redirect('admin/usuario/index');
        }else{
            show_error('The usuario you are trying to delete does not exist.');
        }
    }
    
}
