<?php

class Cliente extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Cliente_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index(){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/cliente/index?');
        $config['total_rows'] = $this->Cliente_model->get_all_clientes_count();
        $this->pagination->initialize($config);

        $data['clientes'] = $this->Cliente_model->get_all_clientes($params);
        
        $data['_view'] = 'admin/cliente/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add(){   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('nombres_cliente','Nombres Cliente','required');
		$this->form_validation->set_rules('apellidos_cliente','Apellidos Cliente','required');
		$this->form_validation->set_rules('email_cliente','Email Cliente','required|valid_email');
		$this->form_validation->set_rules('dni_cliente','Dni Cliente','numeric|required');
		
		if($this->form_validation->run()){   
            $params = array(
				'id_usuario' => $this->input->post('id_usuario'),
				'nombres_cliente' => $this->input->post('nombres_cliente'),
				'apellidos_cliente' => $this->input->post('apellidos_cliente'),
				'email_cliente' => $this->input->post('email_cliente'),
				'dni_cliente' => $this->input->post('dni_cliente'),
            );
            
            $cliente_id = $this->Cliente_model->add_cliente($params);
            redirect('admin/cliente/index');
        }else{
			$this->load->model('Usuario_model');
			$data['all_usuarios'] = $this->Usuario_model->get_all_usuarios();
            
            $data['_view'] = 'admin/cliente/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_cliente){   
        // check if the cliente exists before trying to edit it
        $data['cliente'] = $this->Cliente_model->get_cliente($id_cliente);
        
        if(isset($data['cliente']['id_cliente'])){
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nombres_cliente','Nombres Cliente','required');
			$this->form_validation->set_rules('apellidos_cliente','Apellidos Cliente','required');
			$this->form_validation->set_rules('email_cliente','Email Cliente','required|valid_email');
			$this->form_validation->set_rules('dni_cliente','Dni Cliente','numeric|required');
		
			if($this->form_validation->run()){   
                $params = array(
					'id_usuario' => $this->input->post('id_usuario'),
					'nombres_cliente' => $this->input->post('nombres_cliente'),
					'apellidos_cliente' => $this->input->post('apellidos_cliente'),
					'email_cliente' => $this->input->post('email_cliente'),
					'dni_cliente' => $this->input->post('dni_cliente'),
                );

                $this->Cliente_model->update_cliente($id_cliente,$params);            
                redirect('admin/cliente/index');
            }else{
				$this->load->model('Usuario_model');
				$data['all_usuarios'] = $this->Usuario_model->get_all_usuarios();

                $data['_view'] = 'admin/cliente/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The cliente you are trying to edit does not exist.');
        }
    } 

    function remove($id_cliente){
        $cliente = $this->Cliente_model->get_cliente($id_cliente);

        // check if the cliente exists before trying to delete it
        if(isset($cliente['id_cliente'])){
            $this->Cliente_model->delete_cliente($id_cliente);
            redirect('admin/cliente/index');
        }else{
            show_error('The cliente you are trying to delete does not exist.');
        }
    }
    
}
