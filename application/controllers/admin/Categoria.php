<?php

class Categoria extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Categoria_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index(){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/categoria/index?');
        $config['total_rows'] = $this->Categoria_model->get_all_categorias_count();
        $this->pagination->initialize($config);

        $data['categorias'] = $this->Categoria_model->get_all_categorias($params);
        
        $data['_view'] = 'admin/categoria/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add(){   
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

		$this->form_validation->set_rules('nombre_categoria','Categoria','required');
        $this->form_validation->set_rules('url_categoria','url','required');
        $this->form_validation->set_rules('imagen_categoria','Imágen','required');
		
		if($this->form_validation->run()){   
            $params = array(
				'nombre_categoria' => $this->input->post('nombre_categoria'),
                'url_categoria' => $this->input->post('url_categoria'),
				'descripcion_categoria' => $this->input->post('descripcion_categoria'),
                'imagen_categoria' => $this->input->post('imagen_categoria'),
            );
            
            $categoria_id = $this->Categoria_model->add_categoria($params);
            redirect('admin/categoria/index/'.$categoria_id);
        }else{            
            $data['_view'] = 'admin/categoria/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_categoria){   
        // check if the categoria exists before trying to edit it
        $data['categoria'] = $this->Categoria_model->get_categoria($id_categoria);
        
        if(isset($data['categoria']['id_categoria'])){
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

			$this->form_validation->set_rules('nombre_categoria','Categoria','required');
            $this->form_validation->set_rules('url_categoria','url','required');
            $this->form_validation->set_rules('imagen_categoria','Imágen','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'nombre_categoria' => $this->input->post('nombre_categoria'),
                    'url_categoria' => $this->input->post('url_categoria'),
					'descripcion_categoria' => $this->input->post('descripcion_categoria'),
                    'imagen_categoria' => $this->input->post('imagen_categoria'),
                );

                $this->Categoria_model->update_categoria($id_categoria,$params);            
                redirect('admin/categoria/index/'.$id_categoria);
            }else{
                $data['_view'] = 'admin/categoria/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The categoria you are trying to edit does not exist.');
        }
    } 

    function remove($id_categoria){
        $categoria = $this->Categoria_model->get_categoria($id_categoria);

        // check if the categoria exists before trying to delete it
        if(isset($categoria['id_categoria'])){
            $this->Categoria_model->delete_categoria($id_categoria);
            redirect('admin/categoria/index');
        }else{
            show_error('The categoria you are trying to delete does not exist.');
        }
    }
    
}
