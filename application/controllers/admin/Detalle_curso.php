<?php

class Detalle_curso extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Detalle_curso_model');
        $this->load->model('Curso_model');
        $this->load->model('Duracion_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index( $id_curso = null ){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/detalle_curso/index?');
        $config['total_rows'] = $this->Detalle_curso_model->get_all_detalles_curso_count();
        $this->pagination->initialize($config);
        $data['curso'] = $this->Curso_model->get_curso($id_curso);
        // var_dump($data['curso']);
        // $data['detalles_curso'] = $this->Detalle_curso_model->get_all_detalles_curso($params);
        $data['detalles_curso'] = $this->Detalle_curso_model->get_detalle_cursos($id_curso);
        
        
        $data['_view'] = 'admin/detalle_curso/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add( $id_curso = null ){   
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

		$this->form_validation->set_rules('id_curso','Curso','required');
		$this->form_validation->set_rules('descripcion_curso','Descripción','required');
		$this->form_validation->set_rules('id_duracion','Duración','required');

		if($this->form_validation->run()){   
            $params = array(
				'id_curso' => $this->input->post('id_curso'),
				'descripcion_curso' => $this->input->post('descripcion_curso'),
                'id_duracion' => $this->input->post('id_duracion'),
            );
            
            $detalle_curso_id = $this->Detalle_curso_model->add_detalle_curso($params);
            redirect('admin/detalle_curso/index/'.$id_curso);
        }else{
			$this->load->model('Curso_model');
			$data['id_curso'] = $id_curso;
            $data['all_cursos'] = $this->Curso_model->get_all_cursos();
            $data['duraciones'] = $this->Duracion_model->get_all_duraciones(); 
            
            $data['_view'] = 'admin/detalle_curso/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_detalle_curso){   
        // check if the detalle_curso exists before trying to edit it
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        // var_dump($data['detalle_curso']);
        if(isset($data['detalle_curso']['id_detalle_curso'])){
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

			$this->form_validation->set_rules('id_curso','Curso','required');
			$this->form_validation->set_rules('descripcion_curso','Descripción','required');
            $this->form_validation->set_rules('id_duracion','Duración','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'id_curso' => $this->input->post('id_curso'),
					'descripcion_curso' => $this->input->post('descripcion_curso'),
                    'id_duracion' => $this->input->post('id_duracion'),
                );

                $this->Detalle_curso_model->update_detalle_curso($id_detalle_curso,$params);            
                redirect('admin/detalle_curso/index/'.@$this->input->post('id_curso'));
            }else{
				$this->load->model('Curso_model');
				$data['all_cursos'] = $this->Curso_model->get_all_cursos();
                $data['duraciones'] = $this->Duracion_model->get_all_duraciones(); 

                $data['_view'] = 'admin/detalle_curso/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The detalle_curso you are trying to edit does not exist.');
        }
    } 

    function remove($id_detalle_curso){
        $detalle_curso = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);

        // check if the detalle_curso exists before trying to delete it
        if(isset($detalle_curso['id_detalle_curso'])){
            $this->Detalle_curso_model->delete_detalle_curso($id_detalle_curso);
            redirect('admin/detalle-curso/index');
        }else{
            show_error('The detalle_curso you are trying to delete does not exist.');
        }
    }
    
}
