<?php

class Curso extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Curso_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index(){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/curso/index?');
        $config['total_rows'] = $this->Curso_model->get_all_cursos_count();
        $this->pagination->initialize($config);

        $data['cursos'] = $this->Curso_model->get_all_cursos($params);
        
        $data['_view'] = 'admin/curso/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add(){   
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

		$this->form_validation->set_rules('nombre_curso','Curso','required');
        $this->form_validation->set_rules('url_curso','url','required');
		$this->form_validation->set_rules('id_categoria','Categoria','required');
		
		if($this->form_validation->run()){   
            $params = array(
				'id_categoria' => $this->input->post('id_categoria'),
				'nombre_curso' => $this->input->post('nombre_curso'),
                'url_curso' => $this->input->post('url_curso'),
				'descripcion_curso' => $this->input->post('descripcion_curso'),
            );
            
            $curso_id = $this->Curso_model->add_curso($params);
            redirect('admin/curso/index/'.$curso_id);
        }else{
			$this->load->model('Categoria_model');
			$data['all_categorias'] = $this->Categoria_model->get_all_categorias();
            
            $data['_view'] = 'admin/curso/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_curso){   
        // check if the curso exists before trying to edit it
        $data['curso'] = $this->Curso_model->get_curso($id_curso);
        
        if(isset($data['curso']['id_curso'])){
            $this->load->library('form_validation');
            $this->form_validation->set_message('required', 'El campo %s es obligatorio..!');

			$this->form_validation->set_rules('nombre_curso','Curso','required');
            $this->form_validation->set_rules('url_curso','url','required');
			$this->form_validation->set_rules('id_categoria','Categoria','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'id_categoria' => $this->input->post('id_categoria'),
					'nombre_curso' => $this->input->post('nombre_curso'),
                    'url_curso' => $this->input->post('url_curso'),
					'descripcion_curso' => $this->input->post('descripcion_curso'),
                );

                $this->Curso_model->update_curso($id_curso,$params);            
                redirect('admin/curso/index');
            }else{
				$this->load->model('Categoria_model');
				$data['all_categorias'] = $this->Categoria_model->get_all_categorias();

                $data['_view'] = 'admin/curso/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The curso you are trying to edit does not exist.');
        }
    } 

    function remove($id_curso){
        $curso = $this->Curso_model->get_curso($id_curso);

        // check if the curso exists before trying to delete it
        if(isset($curso['id_curso'])){
            $this->Curso_model->delete_curso($id_curso);
            redirect('admin/curso/index');
        }else{
            show_error('The curso you are trying to delete does not exist.');
        }
    }
    
}
