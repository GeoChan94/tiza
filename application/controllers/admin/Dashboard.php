<?php

class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
        if (!$this->session->userdata("login")) {
			redirect(base_url().'login');
		}
    }

    function index(){
        $data['_view'] = 'admin/dashboard';
        $this->load->view('admin/layouts/main',$data);
        //$this->load->view('page_admin_login');
    }
}
