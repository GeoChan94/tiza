<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Administrador_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index(){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/administrador/index?');
        $config['total_rows'] = $this->Administrador_model->get_all_administradores_count();
        $this->pagination->initialize($config);

        $data['administradores'] = $this->Administrador_model->get_all_administradores($params);
        
        $data['_view'] = 'admin/administrador/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add(){   
        $this->load->library('form_validation');

		$this->form_validation->set_rules('apellidos_administrador','Apellidos Administrador','required');
		$this->form_validation->set_rules('nombres_administrador','Nombres Administrador','required');
		$this->form_validation->set_rules('email_administrador','Email Administrador','valid_email|required');
		
		if($this->form_validation->run()){   
            $params = array(
				'id_usuario' => $this->input->post('id_usuario'),
				'nombres_administrador' => $this->input->post('nombres_administrador'),
				'apellidos_administrador' => $this->input->post('apellidos_administrador'),
				'email_administrador' => $this->input->post('email_administrador'),
            );
            
            $administrador_id = $this->Administrador_model->add_administrador($params);
            redirect('admin/administrador/index');
        }else{
			$this->load->model('Usuario_model');
			$data['all_usuarios'] = $this->Usuario_model->get_all_usuarios();
            
            $data['_view'] = 'admin/administrador/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_administrador){   
        // check if the administrador exists before trying to edit it
        $data['administrador'] = $this->Administrador_model->get_administrador($id_administrador);
        
        if(isset($data['administrador']['id_administrador'])){
            $this->load->library('form_validation');

			$this->form_validation->set_rules('apellidos_administrador','Apellidos Administrador','required');
			$this->form_validation->set_rules('nombres_administrador','Nombres Administrador','required');
			$this->form_validation->set_rules('email_administrador','Email Administrador','valid_email|required');
		
			if($this->form_validation->run()){   
                $params = array(
					'id_usuario' => $this->input->post('id_usuario'),
					'nombres_administrador' => $this->input->post('nombres_administrador'),
					'apellidos_administrador' => $this->input->post('apellidos_administrador'),
					'email_administrador' => $this->input->post('email_administrador'),
                );

                $this->Administrador_model->update_administrador($id_administrador,$params);            
                redirect('admin/administrador/index');
            }else{
				$this->load->model('Usuario_model');
				$data['all_usuarios'] = $this->Usuario_model->get_all_usuarios();

                $data['_view'] = 'admin/administrador/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The administrador you are trying to edit does not exist.');
        }
    } 

    function remove($id_administrador){
        $administrador = $this->Administrador_model->get_administrador($id_administrador);

        // check if the administrador exists before trying to delete it
        if(isset($administrador['id_administrador'])){
            $this->Administrador_model->delete_administrador($id_administrador);
            redirect('admin/administrador/index');
        }else{
            show_error('The administrador you are trying to delete does not exist.');
        }
    }
}
