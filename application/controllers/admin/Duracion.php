<?php

class Duracion extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Duracion_model');
        $this->load->model('Curso_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index(){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/duracion/index?');
        $config['total_rows'] = $this->Duracion_model->get_all_duraciones_count();
        $this->pagination->initialize($config);

        $data['duraciones'] = $this->Duracion_model->get_all_duraciones($params);
        
        $data['_view'] = 'admin/duracion/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add( $id_curso = null ){ 
        $data['id_curso_selected'] = $id_curso;
        $data['curso'] = $this->Curso_model->get_curso(trim($id_curso));  
        $this->load->library('form_validation');

		$this->form_validation->set_rules('nombre_duracion','Nombre Duracion','required');
		$this->form_validation->set_rules('cantidad_duracion','Cantidad Duracion','required|numeric');
		$this->form_validation->set_rules('tipo_duracion','Tipo Duracion','required');
		
		if($this->form_validation->run()){   
            $params = array(
				'tipo_duracion' => $this->input->post('tipo_duracion'),
				'nombre_duracion' => $this->input->post('nombre_duracion'),
				'cantidad_duracion' => $this->input->post('cantidad_duracion'),
            );
            
            $duracion_id = $this->Duracion_model->add_duracion($params);
            //redirect('admin/duracion/index/'.$duracion_id);
            redirect('admin/duracion/index/'.$duracion_id);
        }else{
			$this->load->model('Curso_model');
			$data['all_cursos'] = $this->Curso_model->get_all_cursos();
            
            $data['_view'] = 'admin/duracion/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_duracion){   
        // check if the duracion exists before trying to edit it
        $data['duracion'] = $this->Duracion_model->get_duracion($id_duracion);
        
        if(isset($data['duracion']['id_duracion'])){
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nombre_duracion','Nombre Duracion','required');
			$this->form_validation->set_rules('cantidad_duracion','Cantidad Duracion','required|numeric');
			$this->form_validation->set_rules('tipo_duracion','Tipo Duracion','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'tipo_duracion' => $this->input->post('tipo_duracion'),
					'nombre_duracion' => $this->input->post('nombre_duracion'),
					'cantidad_duracion' => $this->input->post('cantidad_duracion'),
                );

                $this->Duracion_model->update_duracion($id_duracion,$params);            
                redirect('admin/duracion/index');
            }else{
				$this->load->model('Curso_model');
				$data['all_cursos'] = $this->Curso_model->get_all_cursos();

                $data['_view'] = 'admin/duracion/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The duracion you are trying to edit does not exist.');
        }
    } 

    function remove($id_duracion){
        $duracion = $this->Duracion_model->get_duracion($id_duracion);

        // check if the duracion exists before trying to delete it
        if(isset($duracion['id_duracion'])){
            $this->Duracion_model->delete_duracion($id_duracion);
            redirect('admin/duracion/index');
        }else{
            show_error('The duracion you are trying to delete does not exist.');
        }
    }
    
}
