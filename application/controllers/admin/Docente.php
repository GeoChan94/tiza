<?php

class Docente extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Docente_model');
        $this->load->model('Curso_model');
        $this->load->model('Detalle_curso_model');
        $this->load->model('Detalle_curso_has_docente_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index( $id_detalle_curso = null, $id_curso = null ){
        $data['id_curso_selected'] = $id_curso;
        $data['curso'] = $this->Curso_model->get_curso($id_curso);
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        $data['id_detalle_curso'] = $id_detalle_curso;
        $data['id_curso'] = $id_curso; 

        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/docente/index?');
        $config['total_rows'] = $this->Docente_model->get_all_docentes_count();
        $this->pagination->initialize($config);

        $data['docentes'] = $this->Docente_model->get_all_docentes($params);
        
        $data['_view'] = 'admin/docente/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add( $id_detalle_curso = null, $id_curso = null ){ 
          
        $this->load->library('form_validation');

		$this->form_validation->set_rules('nombres_docente','Nombres Docente','required');
		$this->form_validation->set_rules('apellidos_docente','Apellidos Docente','required');
		$this->form_validation->set_rules('especialidad_docente','Especialidad Docente','required');
		$this->form_validation->set_rules('telefono_docente','Telefono Docente','numeric|required');
		$this->form_validation->set_rules('email_docente','Email Docente','required|valid_email');
		
		if($this->form_validation->run()){   
            $params = array(
				'especialidad_docente' => $this->input->post('especialidad_docente'),
				'nombres_docente' => $this->input->post('nombres_docente'),
				'apellidos_docente' => $this->input->post('apellidos_docente'),
				'telefono_docente' => $this->input->post('telefono_docente'),
				'email_docente' => $this->input->post('email_docente'),
            );
            
            $docente_id = $this->Docente_model->add_docente($params);
            $this->Detalle_curso_has_docente_model->add_detalle_curso_has_docente(
                array(
                    "id_detalle_curso"=> $id_detalle_curso,
                    "id_docente" => $docente_id,
                )
            );
            redirect('admin/docente/index/'.$id_detalle_curso.'/'.$id_curso);
        }else{  
            $data['id_curso_selected'] = $id_curso;
            $data['curso'] = $this->Curso_model->get_curso($id_curso);
            $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
            $data['id_detalle_curso'] = $id_detalle_curso;
            $data['id_curso'] = $id_curso; 

            $data['_view'] = 'admin/docente/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit( $id_docente, $id_detalle_curso = null, $id_curso = null ){   
        $data['id_curso_selected'] = $id_curso;
        $data['curso'] = $this->Curso_model->get_curso($id_curso);
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        $data['id_detalle_curso'] = $id_detalle_curso;
        $data['id_curso'] = $id_curso; 
        // check if the docente exists before trying to edit it
        $data['docente'] = $this->Docente_model->get_docente($id_docente);
        
        if(isset($data['docente']['id_docente'])){
            $this->load->library('form_validation');

			$this->form_validation->set_rules('nombres_docente','Nombres Docente','required');
			$this->form_validation->set_rules('apellidos_docente','Apellidos Docente','required');
			$this->form_validation->set_rules('especialidad_docente','Especialidad Docente','required');
			$this->form_validation->set_rules('telefono_docente','Telefono Docente','numeric|required');
			$this->form_validation->set_rules('email_docente','Email Docente','required|valid_email');
		
			if($this->form_validation->run()){   
                $params = array(
					'especialidad_docente' => $this->input->post('especialidad_docente'),
					'nombres_docente' => $this->input->post('nombres_docente'),
					'apellidos_docente' => $this->input->post('apellidos_docente'),
					'telefono_docente' => $this->input->post('telefono_docente'),
					'email_docente' => $this->input->post('email_docente'),
                );

                $this->Docente_model->update_docente($id_docente,$params);            
                redirect('admin/docente/index/'.$id_detalle_curso.'/'.$id_curso);
            }else{
                $data['_view'] = 'admin/docente/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The docente you are trying to edit does not exist.');
        }
    } 

    function remove($id_docente){
        $docente = $this->Docente_model->get_docente($id_docente);

        // check if the docente exists before trying to delete it
        if(isset($docente['id_docente'])){
            $this->Docente_model->delete_docente($id_docente);
            redirect('admin/docente/index');
        }else{
            show_error('The docente you are trying to delete does not exist.');
        }
    }
    
}
