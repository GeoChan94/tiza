<?php

class Archivo extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Archivo_model');
        $this->load->model('Curso_model');
        $this->load->model('Detalle_curso_model');
        $this->load->model('Detalle_curso_has_archivo_model');
        if (!$this->session->userdata("login")) {
            redirect(base_url().'login');
        }
    } 

    function index($id_detalle_curso = null, $id_curso = null ){
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('admin/archivo/index?');
        $config['total_rows'] = $this->Archivo_model->get_all_archivos_count();
        $this->pagination->initialize($config);

        $data['archivos'] = $this->Archivo_model->get_all_archivos($params);
        $data['curso'] = $this->Curso_model->get_curso($id_curso);
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        $data['id_detalle_curso'] = $id_detalle_curso;
        $data['id_curso'] = $id_curso;

        $data['_view'] = 'admin/archivo/index';
        $this->load->view('admin/layouts/main',$data);
    }

    function add( $id_detalle_curso = null , $id_curso = null ){ 
        $data['id_curso_selected'] = $id_curso;
        $data['curso'] = $this->Curso_model->get_curso(trim($id_curso));

        $this->load->library('form_validation');

		$this->form_validation->set_rules('txt_archivo','Archivo','required');
		$this->form_validation->set_rules('nombre_archivo','Nombre Archivo','required');
		$this->form_validation->set_rules('tipo_archivo','Tipo Archivo','required');
		
		if($this->form_validation->run()){   
            $params = array(
				'descarga_archivo' => $this->input->post('descarga_archivo')?1:0,
				'tipo_archivo' => $this->input->post('tipo_archivo'),
				'uri_archivo' => $this->input->post('txt_archivo'),
				'nombre_archivo' => $this->input->post('nombre_archivo'),
				'descripcion_archivo' => $this->input->post('descripcion_archivo'),
            );
            
            $archivo_id = $this->Archivo_model->add_archivo($params);
            $this->Detalle_curso_has_archivo_model->add_detalle_curso_has_archivo(
                array(
                    'id_detalle_curso' => $id_detalle_curso,
                    'id_archivo' => $archivo_id,
                )
            );
            //redirect('admin/archivo/index');
            redirect('admin/archivo/index/'.$id_detalle_curso.'/'.$id_curso);
        }else{   
            $data['curso'] = $this->Curso_model->get_curso($id_curso);
            $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);         
            $data['_view'] = 'admin/archivo/add';
            $this->load->view('admin/layouts/main',$data);
        }
    }  

    function edit($id_archivo, $id_detalle_curso = null , $id_curso = null ){   
        // check if the archivo exists before trying to edit it
        $data['archivo'] = $this->Archivo_model->get_archivo($id_archivo);
        $data['curso'] = $this->Curso_model->get_curso($id_curso);
        $data['detalle_curso'] = $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
        
        if(isset($data['archivo']['id_archivo'])){
            $this->load->library('form_validation');

			$this->form_validation->set_rules('txt_archivo','Archivo','required');
			$this->form_validation->set_rules('nombre_archivo','Nombre Archivo','required');
			$this->form_validation->set_rules('tipo_archivo','Tipo Archivo','required');
		
			if($this->form_validation->run()){   
                $params = array(
					'descarga_archivo' => $this->input->post('descarga_archivo')?1:0,
					'tipo_archivo' => $this->input->post('tipo_archivo'),
					'uri_archivo' => $this->input->post('txt_archivo'),
					'nombre_archivo' => $this->input->post('nombre_archivo'),
					'descripcion_archivo' => $this->input->post('descripcion_archivo'),
                );

                $this->Archivo_model->update_archivo($id_archivo,$params);            
                redirect('admin/archivo/index/'.$id_detalle_curso.'/'.$id_curso);
            }else{
                $data['_view'] = 'admin/archivo/edit';
                $this->load->view('admin/layouts/main',$data);
            }
        }else{
            show_error('The archivo you are trying to edit does not exist.');
        }
    } 

    function remove($id_archivo){
        $archivo = $this->Archivo_model->get_archivo($id_archivo);

        // check if the archivo exists before trying to delete it
        if(isset($archivo['id_archivo'])){
            $this->Archivo_model->delete_archivo($id_archivo);
            redirect('admin/archivo/index');
        }else{
            show_error('The archivo you are trying to delete does not exist.');
        }
    }
}
