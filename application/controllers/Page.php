<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('Categoria_model');
        $this->load->model('Curso_model');
        $this->load->model('Detalle_curso_model');
        $this->load->model('Archivo_model');
        $this->load->model('Horario_model');
        $this->load->model('Docente_model');
    } 

	public function index(){
		$data[]='';
		
		$all_cateorias= $this->Categoria_model->get_all_categorias();
		// var_dump($all_cateorias);
		foreach ($all_cateorias as $key => $value) {
			$data['cateorias'][] = array(
				'id_categoria'=>$value['id_categoria'],
				'nombre_categoria' => $value['nombre_categoria'],
				'imagen_categoria' => $value['imagen_categoria'],
				'url_categoria' => base_url().'categoria/'.strtolower($value['url_categoria']),
				'url_imagen' => base_url().'assets/archivos/'.$value['imagen_categoria'],
			 );
		}
		// echo json_encode($data);
		$this->load->view('page_index',$data);
		
	}
	public function page_curso($uri){
		$data[]='';
		
		$curso= $this->Curso_model->get_curso_uri($uri);
		// var_dump($curso);
		$temp_contenido_curso=$this->Detalle_curso_model->get_detalle_cursos($curso['id_curso']);
		// var_dump($temp_contenido_curso);
		foreach ($temp_contenido_curso as $key => $value) {
			$data['contenido_curso'][]=array(
				'descripcion_curso' =>$value['descripcion_curso'], 
				'semana' =>($key+1), 
				'id_detalle_curso' =>$value['id_detalle_curso'], 
				'url_curso_video' =>base_url().'curso/'.$value['url_curso'].'/'.$value['id_detalle_curso'],
			);
		}
		// var_dump($data['contenido_curso']);
		$data['curso']=$curso;
		$this->load->view('page_curso',$data);
	}

	public function page_categoria($uri){
		$data=array();
		$all_cursos= $this->Categoria_model->get_categoria_has_curso($uri);
		// var_dump($all_cursos);
		$categoria=$this->Categoria_model->get_categoria_uri($uri);
		// var_dump($categoria);
		$data['categoria']=$categoria;
		// var_dump($categoria);
		foreach ($all_cursos as $key => $value) {
			$data['cursos'][] = array(
				'imagen_categoria' => $data['categoria']['imagen_categoria'],
				'url_curso' => base_url().'curso/'.$value['url_curso'],
				'url_imagen' => base_url().'assets/archivos/'.$data['categoria']['imagen_categoria'],
				'nombre_curso' => $value['nombre_curso'],
				'descripcion_curso' => $value['descripcion_curso'],
			 );
		}
		// var_dump($data['cursos']);
		$this->load->view('page_categoria',$data);
	}
	public function page_details($nombre_curso,$id_detalle_curso){
		$data=array();
		$curso= $this->Detalle_curso_model->get_detalle_curso($id_detalle_curso);
		$data['curso']=$curso;
		$data['archivos'] = $this->Archivo_model->get_archivosByDetalleCurso($data['curso']['id_curso'], $data['curso']['id_detalle_curso']);
		$data['horarios'] = $this->Horario_model->get_horariosByDetalleCurso($data['curso']['id_curso'], $data['curso']['id_detalle_curso']);
		$data['docentes'] = $this->Docente_model->get_docentesByDetalleCurso($data['curso']['id_curso'], $data['curso']['id_detalle_curso']);

 		echo json_encode($data);
		$this->load->view('page_details',$data);
	}
}
