-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 16-06-2018 a las 03:13:09
-- Versión del servidor: 10.2.9-MariaDB
-- Versión de PHP: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ivonnecl_tiza`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_administrador` int(11) NOT NULL,
  `nombres_administrador` varchar(128) DEFAULT NULL,
  `apellidos_administrador` varchar(128) DEFAULT NULL,
  `email_administrador` varchar(64) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_administrador`, `nombres_administrador`, `apellidos_administrador`, `email_administrador`, `id_usuario`) VALUES
(1, 'admin', 'admin', 'admin@tiza.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `archivo`
--

CREATE TABLE `archivo` (
  `id_archivo` int(11) NOT NULL,
  `uri_archivo` varchar(128) DEFAULT NULL,
  `nombre_archivo` varchar(255) DEFAULT NULL,
  `descripcion_archivo` text DEFAULT NULL,
  `tipo_archivo` varchar(64) DEFAULT NULL COMMENT 'vídeo, audio, pdf, etc',
  `descarga_archivo` tinyint(4) DEFAULT NULL COMMENT '0 = No descargable\n1 = Descargable'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(255) DEFAULT NULL,
  `url_categoria` varchar(255) NOT NULL,
  `descripcion_categoria` text DEFAULT NULL,
  `imagen_categoria` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre_categoria`, `url_categoria`, `descripcion_categoria`, `imagen_categoria`) VALUES
(1, 'PRE-U','pre-u', 'PRE-U', 'equipamiento.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL,
  `nombres_cliente` varchar(128) DEFAULT NULL,
  `apellidos_cliente` varchar(128) DEFAULT NULL,
  `email_cliente` varchar(64) DEFAULT NULL,
  `dni_cliente` varchar(8) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_has_curso`
--

CREATE TABLE `cliente_has_curso` (
  `id_cliente` int(11) NOT NULL,
  `id_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `id_curso` int(11) NOT NULL,
  `nombre_curso` varchar(45) DEFAULT NULL,
  `url_curso` varchar(45) NOT NULL,
  `descripcion_curso` varchar(45) DEFAULT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`id_curso`, `nombre_curso`,`url_curso`, `descripcion_curso`, `id_categoria`) VALUES
(1, 'Matemáticas I', 'Matemáticas I','matemáticas-i', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_has_docente`
--

CREATE TABLE `curso_has_docente` (
  `id_curso` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp850;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso_has_horario`
--

CREATE TABLE `curso_has_horario` (
  `id_curso` int(11) NOT NULL,
  `id_horario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_curso`
--

CREATE TABLE `detalle_curso` (
  `id_detalle_curso` int(11) NOT NULL,
  `descripcion_curso` text DEFAULT NULL,
  `id_curso` int(11) NOT NULL,
  `id_duracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_curso`
--

INSERT INTO `detalle_curso` (`id_detalle_curso`, `descripcion_curso`, `id_curso`, `id_duracion`) VALUES
(1, 'Introducción a matemática I', 1, 1),
(2, 'matimáticas para universitarios', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_curso_has_archivo`
--

CREATE TABLE `detalle_curso_has_archivo` (
  `id_detalle_curso` int(11) NOT NULL,
  `id_archivo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_curso_has_docente`
--

CREATE TABLE `detalle_curso_has_docente` (
  `id_detalle_curso` int(11) NOT NULL,
  `id_docente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_curso_has_docente`
--

INSERT INTO `detalle_curso_has_docente` (`id_detalle_curso`, `id_docente`) VALUES
(2, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_curso_has_horario`
--

CREATE TABLE `detalle_curso_has_horario` (
  `id_detalle_curso` int(11) NOT NULL,
  `id_horario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_curso_has_horario`
--

INSERT INTO `detalle_curso_has_horario` (`id_detalle_curso`, `id_horario`) VALUES
(2, 1),
(2, 2),
(2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `id_docente` int(11) NOT NULL,
  `nombres_docente` varchar(128) DEFAULT NULL,
  `apellidos_docente` varchar(128) DEFAULT NULL,
  `especialidad_docente` varchar(128) DEFAULT NULL,
  `telefono_docente` varchar(16) DEFAULT NULL,
  `email_docente` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`id_docente`, `nombres_docente`, `apellidos_docente`, `especialidad_docente`, `telefono_docente`, `email_docente`) VALUES
(1, 'carlos', 'morales', '2', '8348345839', 'carlos@tiza.com'),
(2, 'marcos', 'peralta', '2', '454384967', 'peralta@tiza.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `duracion`
--

CREATE TABLE `duracion` (
  `id_duracion` int(11) NOT NULL,
  `nombre_duracion` varchar(128) DEFAULT NULL,
  `cantidad_duracion` int(11) DEFAULT NULL COMMENT 'cantidad numérica',
  `tipo_duracion` varchar(64) DEFAULT NULL COMMENT 'min = minutos,\nhor = horas,\ndia = días,\n mes = meses,\nyea = años'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `duracion`
--

INSERT INTO `duracion` (`id_duracion`, `nombre_duracion`, `cantidad_duracion`, `tipo_duracion`) VALUES
(1, 'Primera Semana', 1, '4'),
(2, 'Segunda Semana', 1, '4'),
(3, 'Tercera Semana', 1, '4'),
(4, 'Primer Mes', 1, '5'),
(5, 'Segundo Mes', 1, '5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `id_horario` int(11) NOT NULL,
  `nombre_horario` varchar(128) DEFAULT NULL,
  `cantidad_horario` int(11) DEFAULT NULL,
  `tipo_horario` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`id_horario`, `nombre_horario`, `cantidad_horario`, `tipo_horario`) VALUES
(1, '7:30AM-9:30AM', 2, '2'),
(2, '9:30AM-11:30AM', 2, '2'),
(3, '11:30AM-13:30PM', 2, '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario_has_detalle_curso`
--

CREATE TABLE `horario_has_detalle_curso` (
  `horario_id_horario` int(11) NOT NULL,
  `detalle_curso_id_detalle_curso` int(11) NOT NULL,
  `detalle_curso_id_duracion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `usuario`, `password`) VALUES
(1, 'admin', 'admin'),
(2, 'usuario', 'usuario');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id_administrador`),
  ADD KEY `fk_administrador_usuario1_idx` (`id_usuario`);

--
-- Indices de la tabla `archivo`
--
ALTER TABLE `archivo`
  ADD PRIMARY KEY (`id_archivo`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`),
  ADD KEY `fk_cliente_usuario_idx` (`id_usuario`);

--
-- Indices de la tabla `cliente_has_curso`
--
ALTER TABLE `cliente_has_curso`
  ADD PRIMARY KEY (`id_cliente`,`id_curso`),
  ADD KEY `fk_cliente_has_curso_curso1_idx` (`id_curso`),
  ADD KEY `fk_cliente_has_curso_cliente1_idx` (`id_cliente`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id_curso`),
  ADD KEY `fk_curso_categoria1_idx` (`id_categoria`);

--
-- Indices de la tabla `curso_has_docente`
--
ALTER TABLE `curso_has_docente`
  ADD PRIMARY KEY (`id_curso`,`id_docente`),
  ADD KEY `fk_curso_has_docente_docente1_idx` (`id_docente`),
  ADD KEY `fk_curso_has_docente_curso1_idx` (`id_curso`);

--
-- Indices de la tabla `curso_has_horario`
--
ALTER TABLE `curso_has_horario`
  ADD PRIMARY KEY (`id_curso`,`id_horario`),
  ADD KEY `fk_curso_has_horario_horario1_idx` (`id_horario`),
  ADD KEY `fk_curso_has_horario_curso1_idx` (`id_curso`);

--
-- Indices de la tabla `detalle_curso`
--
ALTER TABLE `detalle_curso`
  ADD PRIMARY KEY (`id_detalle_curso`,`id_duracion`),
  ADD KEY `fk_detalle_curso_curso1_idx` (`id_curso`),
  ADD KEY `fk_detalle_curso_duracion1_idx` (`id_duracion`);

--
-- Indices de la tabla `detalle_curso_has_archivo`
--
ALTER TABLE `detalle_curso_has_archivo`
  ADD PRIMARY KEY (`id_detalle_curso`,`id_archivo`),
  ADD KEY `fk_detalle_curso_has_archivo_archivo1_idx` (`id_archivo`),
  ADD KEY `fk_detalle_curso_has_archivo_detalle_curso1_idx` (`id_detalle_curso`);

--
-- Indices de la tabla `detalle_curso_has_docente`
--
ALTER TABLE `detalle_curso_has_docente`
  ADD PRIMARY KEY (`id_detalle_curso`,`id_docente`),
  ADD KEY `fk_detalle_curso_has_docente_docente1_idx` (`id_docente`),
  ADD KEY `fk_detalle_curso_has_docente_detalle_curso1_idx` (`id_detalle_curso`);

--
-- Indices de la tabla `detalle_curso_has_horario`
--
ALTER TABLE `detalle_curso_has_horario`
  ADD PRIMARY KEY (`id_detalle_curso`,`id_horario`),
  ADD KEY `fk_detalle_curso_has_horario_horario1_idx` (`id_horario`),
  ADD KEY `fk_detalle_curso_has_horario_detalle_curso1_idx` (`id_detalle_curso`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`id_docente`);

--
-- Indices de la tabla `duracion`
--
ALTER TABLE `duracion`
  ADD PRIMARY KEY (`id_duracion`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`id_horario`);

--
-- Indices de la tabla `horario_has_detalle_curso`
--
ALTER TABLE `horario_has_detalle_curso`
  ADD PRIMARY KEY (`horario_id_horario`,`detalle_curso_id_detalle_curso`,`detalle_curso_id_duracion`),
  ADD KEY `fk_horario_has_detalle_curso_detalle_curso1_idx` (`detalle_curso_id_detalle_curso`,`detalle_curso_id_duracion`),
  ADD KEY `fk_horario_has_detalle_curso_horario1_idx` (`horario_id_horario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id_administrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `archivo`
--
ALTER TABLE `archivo`
  MODIFY `id_archivo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `id_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle_curso`
--
ALTER TABLE `detalle_curso`
  MODIFY `id_detalle_curso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `docente`
--
ALTER TABLE `docente`
  MODIFY `id_docente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `duracion`
--
ALTER TABLE `duracion`
  MODIFY `id_duracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `id_horario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD CONSTRAINT `fk_administrador_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_cliente_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cliente_has_curso`
--
ALTER TABLE `cliente_has_curso`
  ADD CONSTRAINT `fk_cliente_has_curso_cliente1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cliente_has_curso_curso1` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `curso`
--
ALTER TABLE `curso`
  ADD CONSTRAINT `fk_curso_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `curso_has_docente`
--
ALTER TABLE `curso_has_docente`
  ADD CONSTRAINT `fk_curso_has_docente_curso1` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_curso_has_docente_docente1` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `curso_has_horario`
--
ALTER TABLE `curso_has_horario`
  ADD CONSTRAINT `fk_curso_has_horario_curso1` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_curso_has_horario_horario1` FOREIGN KEY (`id_horario`) REFERENCES `horario` (`id_horario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalle_curso`
--
ALTER TABLE `detalle_curso`
  ADD CONSTRAINT `fk_detalle_curso_curso1` FOREIGN KEY (`id_curso`) REFERENCES `curso` (`id_curso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detalle_curso_duracion1` FOREIGN KEY (`id_duracion`) REFERENCES `duracion` (`id_duracion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_curso_has_archivo`
--
ALTER TABLE `detalle_curso_has_archivo`
  ADD CONSTRAINT `fk_detalle_curso_has_archivo_archivo1` FOREIGN KEY (`id_archivo`) REFERENCES `archivo` (`id_archivo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detalle_curso_has_archivo_detalle_curso1` FOREIGN KEY (`id_detalle_curso`) REFERENCES `detalle_curso` (`id_detalle_curso`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalle_curso_has_docente`
--
ALTER TABLE `detalle_curso_has_docente`
  ADD CONSTRAINT `fk_detalle_curso_has_docente_detalle_curso1` FOREIGN KEY (`id_detalle_curso`) REFERENCES `detalle_curso` (`id_detalle_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_curso_has_docente_docente1` FOREIGN KEY (`id_docente`) REFERENCES `docente` (`id_docente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalle_curso_has_horario`
--
ALTER TABLE `detalle_curso_has_horario`
  ADD CONSTRAINT `fk_detalle_curso_has_horario_detalle_curso1` FOREIGN KEY (`id_detalle_curso`) REFERENCES `detalle_curso` (`id_detalle_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_curso_has_horario_horario1` FOREIGN KEY (`id_horario`) REFERENCES `horario` (`id_horario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `horario_has_detalle_curso`
--
ALTER TABLE `horario_has_detalle_curso`
  ADD CONSTRAINT `fk_horario_has_detalle_curso_detalle_curso1` FOREIGN KEY (`detalle_curso_id_detalle_curso`,`detalle_curso_id_duracion`) REFERENCES `detalle_curso` (`id_detalle_curso`, `id_duracion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_horario_has_detalle_curso_horario1` FOREIGN KEY (`horario_id_horario`) REFERENCES `horario` (`id_horario`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
