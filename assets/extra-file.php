<?php
$BASE_URL  ="http://localhost/tiza/";
$data = null;
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    //$file = $_FILES['archivos']['name'];
    //$nameFile = $_FILES['archivos']['name'];
    //$extension = pathinfo($_FILES['archivos']['name'],PATHINFO_EXTENSION);
    $extension = pathinfo($_FILES['archivos']['name']); 
    $file = "video-".date('Ymd-His').".".$extension['extension'];

    if(!is_dir("archivos/")){ 
      mkdir("archivos/", 0777);
      //mkdir("imagenes/categoria/", 0777);
    }
    if ($file && move_uploaded_file($_FILES['archivos']['tmp_name'],"archivos/".$file)){
      //sleep(2);
      $data = array(
        'rpta'    => 'success',
        'message' => 'Successful image',
        'imagen'  => $file,
        'url'     => $BASE_URL.'assets/archivos/'.$file,
      );
    }else{
      $data = array(
        'rpta' => 'error',
        'message' => 'The image cann\'t be uploaded',
        'url'  => $BASE_URL.'assets/img/error.png',
      );
    }
}else{
    //throw new Exception("Error Processing Request", 1);  
    $data = array(
      'rpta' => 'error',
      'message' => 'Error connecting to server',
      'url'  => $BASE_URL.'assets/img/error.png',
    ); 
}

echo json_encode($data);
/*
	$uploaddir = '../imagenes/';
	if (!is_dir($uploaddir)) {
   		mkdir($uploaddir, 0777);
	}
	$file =  $_FILES['archivos']['name'];
	$uploadfile = $uploaddir . $file;
	$data = array();
	if (move_uploaded_file($_FILES['archivos']['tmp_name'], $uploadfile)) {				
                $data = array(
                    'rpta' => 'OK',
                    'imagen'  => $file,
                    'url'  => 'http://incalake.com/control/public/imagenes/'.$file,
                );
	} else {
                $data = array(
                   'rpta' => 'ERROR',
                   'img'  => 'http://incalake.com/control/public/imagenes/error.png',
                   'url'  => 'http://incalake.com/control/public/imagenes/error.png',
                ); 
	}
	echo json_encode($data);
  */
?>